/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
* No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
* applicable laws, including copyright laws. 
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIESREGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED
* OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY
* LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR
* ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability 
* of this software. By using this software, you agree to the additional terms and conditions found by accessing the 
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2011, 2014 Renesas Electronics Corporation. All rights reserved.
***********************************************************************************************************************/

/***********************************************************************************************************************
* File Name    : r_cg_serial.h
* Version      : CodeGenerator for RL78/G13 V2.03.00.03 [07 Aug 2014]
* Device(s)    : R5F100FA
* Tool-Chain   : CA78K0R
* Description  : This file implements device driver for Serial module.
* Creation Date: 22-04-2015
***********************************************************************************************************************/

#ifndef LCD_H
#define LCD_H

#include "r_cg_macrodriver.h"
#include "r_cg_userdefine.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/

#define LCD_D1		P7.2
#define LCD_D2		P7.3
#define LCD_D3		P7.4
#define LCD_D4		P7.5

#define LCD_RS		P7.0
#define LCD_EN		P7.1

#define LCD_BL		P7.7 //LCD BACKLIGHT
#define LCD_RW          P7.6

//LCD ctrl config
#define LCD_CTRL_REG    0
#define LCD_DATA_REG    1
//LCD Commands
#define  LCD_CLR_SCR    0x01
#define  LCD_RET_HOME   0x02
#define  LCD_CRSR_LFT   0x03
#define  LCD_CRSR_RHT   0x04
#define  LCD_ENT_MODE   0x06
#define  LCD_DISP_OFF   0x08   
#define  LCD_CURSOR_ON  0x0E
#define  LCD_CURSOR_OFF 0x0C
#define  LCD_CRSR_BLINK 0x0F
#define  LCD_SHFT_LEFT  0x18
#define  LCD_SHFT_RHT   0x1C
#define  LCD_CHAR_LEFT  0x10   
#define  LCD_CHAR_RHT   0x14

#define MAX_LCD_TXT_LEN 16

typedef enum
{
	BATT_NORM=0,
	BATT_LOW,
	BATT_SHUT,
	SMS_WAIT,
	SMS_SUCC,
	SMS_FAIL,
	UI_CTRL,
	KEY_CTRL,
	DISABLE,
	SIREN_OFF,
	NO_SIM,
	E_ICON_END
}E_ICON_ARRAY;


/***********************************************************************************************************************
Typedef definitions
***********************************************************************************************************************/

/***********************************************************************************************************************
Global functions
***********************************************************************************************************************/
void lcd_init(void);
void lcd_sendByte(uint8_t ui8Addr, uint8_t ui8Data);
void lcd_goToXy(uint8_t ui8X, uint8_t ui8Y);
void lcd_putC(char cChar);
void lcd_printf(char *cStr);
//void lcd_writeCustomDesgn( char* cData, uint8_t ui8Loc);
void lcd_clearLines( uint8_t ui8StrtCnt, uint8_t ui8EndCnt, uint8_t ui8LocX, uint8_t ui8LocY);
void lcd_cursor_on(void);
void lcd_cursor_off(void);
void lcd_blink_on(void);
void lcd_blink_off(void);
void lcd_clear_screen(void);
//void lcd_printnum(uint16_t num,uint8_t print_len);
void lcd_printnum(uint8_t x,uint8_t y,uint16_t num,uint8_t print_len);
void lcd_PrintSP(char *cStr);
void lcd_PrintSP_d(char *cStr,uint8_t len);
//void vLCD_BackLight_ctl(G_CTL_CODES ctl);
void vLCD_BackLight_ctl(G_CTL_CODES ctl);
/******************custom_desgn functions*******************************************/

void show_icon(E_ICON_ARRAY icon_index,uint8_t col,uint8_t row);
void scrolltext();
uint8_t lcd_read_char(uint8_t line,char *main_str,uint8_t main_str_len);
void lcd_hdlr(void);
G_RESP_CODE lcd_update_hdlr(uint8_t x,uint8_t y,char *str,uint8_t len);
/* Start user code for function. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */

#endif
