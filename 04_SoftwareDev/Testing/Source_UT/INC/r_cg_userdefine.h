/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
* No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
* applicable laws, including copyright laws. 
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIESREGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED
* OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY
* LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR
* ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability 
* of this software. By using this software, you agree to the additional terms and conditions found by accessing the 
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2011, 2015 Renesas Electronics Corporation. All rights reserved.
***********************************************************************************************************************/

/***********************************************************************************************************************
* File Name    : r_cg_userdefine.h
* Version      : CodeGenerator for RL78/G13 V2.03.02.01 [15 May 2015]
* Device(s)    : R5F100LE
* Tool-Chain   : CA78K0R
* Description  : This file includes user definition.
* Creation Date: 2/3/2019
***********************************************************************************************************************/

#ifndef _USER_DEF_H
#define _USER_DEF_H

/***********************************************************************************************************************
User definitions
***********************************************************************************************************************/

/* Start user code for function. Do not edit comment generated here */
#include "string.h"
#include "stdio.h"
//#define RTC_TEST 0
//#define E2P_CRI_TEST 0
//#define GSM_UART_SWITCH 0




#define G_RESP_CODE_MODE_OFFSET 56
#define STARTUP_E2P_CLEAR 0
#define ZERO_ASCII_VAL 48
#define MAX_U16_VAL 30000
typedef enum
{
	DEFAULT = 0,
	SUCCESS = 1,//refer M66 M_GSM_cmd_send()
	FAILURE,
	WAITING,
	INVALID_PARAM,
	EXIT,
	DATA_AVAILABLE,
	GSM_RXBUFF_FULL,
	CALL_ESTB,
	CALL_ANS,
	CALL_HANG,
	CALL_ANS_ERROR,
}G_RESP_CODE;
typedef enum
{
	OFF = 0, // OFF REPRESENTS 0V SO DONT  CHANGE THE EENUM VALUE
	ON = 1,  // ON REPRESENTS 5V SO DONT  CHANGE THE EENUM VALUE
	NILL = 2,
	RESET = 7,	
    	TOGGLE=12,
}G_CTL_CODES;
#define SL_ADDR         (0xA0U)         /* Slave address */

 
/***************EEPROM iNDEXDEFINITIONS START*******************/

#define PHONE_NO_START_INDEX 0
#define PHONE_NO_OFFSET 15

#define PH_NO_STATUS_START_INDEX 150
#define PH_NO_STATUS_OFFSET 1

#define PWD_START_INDEX 160
#define PWD_OFFSET 2

#define ZONE_SETTINGS_START_INDEX 162
#define ZONE_SETTINGS_OFFSET 5

#define A_I_START_INDEX 162
#define D_N_START_INDEX 163
#define NO_NC_I_START_INDEX 164
#define DELAY_START_INDEX 165

#define SIREN_STATE_START_INDEX 182//222
#define SIREN_STATE_OFFSET 1

#define SIREN_TIME_START_INDEX 183//223
#define SIREN_TIME_OFFSET 2

#define MODE_CTRL_CONFIG_START_INDEX 185//225
#define MODE_CTRL_CONFIG_OFFSET 1

#define CURRENT_SYS_MODE_START_INDEX 186//226
#define CURRENT_SYS_MODE_OFFSET 1

#define ZONE_STS_INDEX 187
#define ZONE_STS_OFFSET 5 //4 + 1

#define CURR_E2P_PAGE 187//227
#define CURR_E2P_PAGE_OFFSET 2

#define REF_RTC_YEAR 188
#define REF_RTC_MONTH 189
#define REF_RTC_DAY 190
#define REF_RTC_HOUR 191
#define REF_RTC_MINUTE 192
#define REF_RTC_OFFSET 5

#define MAXEEBUFFLENGTH 256
#define MAX_EEPROM_BUFF_SIZE 200
/***************EEPROM iNDEXDEFINITIONS STOP*******************/

typedef enum
{
	ISOLATE = 0,
	ACTIVATE = 1,
	ACTIVE_STS_END
}ACTIVE_STS;

typedef enum
{
	E_DEFAULT = 0,	
	E_ENTER_UI,
	E_KEY_CHANGE,
	E_POWER,
	E_ALT_STOP,
	E_ALT_STOP_SMS,
	E_ALT_TRIG,
	E_SMS_TRIG,
	E_REC_START,
	E_REC_STOP,
	E_CALL_START,
	E_CALL_STOP,
	E_MODE,
	E_E2P_SEND,
	E_CALL_STS,
	/**********##TBH##************/
	E_FRST,
	E_UI_IN,
	E_UI_OUT,
	E_GSM_STS,
	
	
	E_E2P_CRI_STORE,
	E_E2P_CRI_READ,
	E_E2P_LOG_STORE,
	E_RTC_RETRY_STS,
	E_DISARM_KEY,
	E_END	
}E_SYS_EVENTS;
/* End user code. Do not edit comment generated here */
#endif
