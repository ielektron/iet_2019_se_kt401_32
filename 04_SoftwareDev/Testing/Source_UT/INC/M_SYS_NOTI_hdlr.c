/***********************************************************************************************************************
Start
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/

#include "M_SYS_NOTI_hdlr.h"
#include "M_led_handler.h" 
#include "PIN_CFG.h"
#include "M_event_handler.h"
#include "A_UI_handler.h"
#include "M_ADC_hdlr.h"
#include "M_power_handler.h"
#include "H_lcd_driver.h"

/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/

void M_NOTI_INTbuzzCtl(G_CTL_CODES ctl);
 
/***********************************************************************************************************************
Extern variables
***********************************************************************************************************************/

/***********************************************************************************************************************
* Function Name: NOTI_hdlr
* Description  : This function control the external peripherals based on the event passed as argument.
* Arguments    : event - any one value of enum E_SYS_EVENTS
				 status - event status
* Return Value : any one value of G_RESP_CODE
***********************************************************************************************************************/

 G_RESP_CODE NOTI_hdlr(E_SYS_EVENTS event,uint8_t status)
{
	G_RESP_CODE ret = SUCCESS; 
	switch(event)
	{
		case E_ALT_STOP:
		{
			M_NOTI_RELAYCtl(REL_SOUNDER,OFF);
			//M_NOTI_RELAYCtl(REL_DIALER,OFF);
			m_u8LED_ctrl_led(LED_ALERT,OFF);
			break;
		}
		case E_ALT_TRIG:
		{
			if(sys_data.u8_G_siren_state == ON)
			{
				M_NOTI_RELAYCtl(REL_SOUNDER,ON);
			}			
			//M_NOTI_RELAYCtl(REL_DIALER,ON);
			m_u8LED_ctrl_led(LED_ALERT,ON);
			break;
		}
		case E_POWER:
		{
			switch(status)
			{
				case BAT_NORM:
				{
					m_u8LED_ctrl_led(LED_AC,OFF);
					m_u8LED_ctrl_led(LED_BAT_GOOD,ON);
					m_u8LED_ctrl_led(LED_BAT_BAD,OFF);
					break;
				}
				case BAT_SHUT:
				case BAT_LOW:
				{
					m_u8LED_ctrl_led(LED_AC,OFF);
					m_u8LED_ctrl_led(LED_BAT_BAD,ON);
					m_u8LED_ctrl_led(LED_BAT_GOOD,OFF);
					break;
				}
				case POW_AC:
				{
					m_u8LED_ctrl_led(LED_AC,ON);
					//m_u8LED_ctrl_led(LED_BAT_BAD,OFF);
					//m_u8LED_ctrl_led(LED_BAT_GOOD,OFF); 
					break;
				}
				case BAT_DISCONN:
				{
					m_u8LED_ctrl_led(LED_BAT_GOOD,OFF);
					m_u8LED_ctrl_led(LED_BAT_BAD,OFF);
					break;
				}
				case BAT_CONN:
				{
					m_u8LED_ctrl_led(LED_BAT_GOOD,ON);
					m_u8LED_ctrl_led(LED_BAT_BAD,OFF);
					break;
				}
				default:
				break;
			}
			break;
		}
		default:
		break;
	}	 
	return ret;
} 

/***********************************************************************************************************************
* Function Name: M_NOTI_RELAYCtl
* Description  : This function turn on or off the relay specified in functions first argument.
* Arguments    : type - any one value of enum RELAY_TYPE_T
				 ctl - on/off
* Return Value : None
***********************************************************************************************************************/

/*void M_NOTI_RELAYCtl(RELAY_TYPE_T type,G_CTL_CODES ctl)
{
	if(type == REL_SOUNDER)
	{
		RELAY_SOUNDER = ctl;
	}
	else if(type == REL_DIALER)
	{
		RELAY_EXT = ctl;
	}	
} */
/*void M_NOTI_INTbuzzCtl(G_CTL_CODES ctl)
{
	INT_BUZZ = ctl;	 
}*/

/***********************************************************************************************************************
End
***********************************************************************************************************************/