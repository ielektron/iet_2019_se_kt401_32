#ifndef PIN_CFG_H
#define PIN_CFG_H
 



#define RELAY_SOUNDER P4.1
#define RELAY_EXT P4.2
#define INT_BUZZ P4.3
#define INT_BUZ P0.1

#define GSM_PWR_KEY P1.0
#define GSM_LDO_ENABLE P3.1
 
//new
#define LED_SHIFT_LATCH P14.1
#define LED_SHIFT_CLOCK P14.0
#define LED_SHIFT1_DIN P14.7


#define ZONE_1_FIRE_ENABLE P12.0
#define ZONE_2_FIRE_ENABLE P3.0

#define ZONE1_IN P2.4 
#define ZONE2_IN P2.5
#define ZONE3_IN P2.6
#define ZONE4_IN P2.7

#define TAMPER P6.3 

#define LATCH_PIN P14.1
#define CLOCK_PIN P14.0
 
#define DATA_PIN1 P14.6

#define EEPROM_CS P13.0
#endif

