#ifndef I_DELAY_H
#define I_DELAY_H



void DelayMs(uint8_t del_val);
void DelayUs(uint16_t del_val);
void h_vDEL_Ms(uint16_t del_val);
void h_vDEL_100nano(uint32_t val);
#endif