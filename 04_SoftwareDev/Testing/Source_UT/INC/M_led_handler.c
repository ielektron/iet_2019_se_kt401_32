#include "M_led_handler.h" 
#include "H_LED_driver.h" 
 
#define LED_TEST_INT 20
 

#define NO_OF_ROWS 1


#define _80_SYS_ALT_LED_BIT 0x80
#define _40_SYS_MUTE_LED_BIT 0x40
#define _80_SYS_NIGHT_LED_BIT 0x80
#define _40_SYS_DAY_LED_BIT 0x40
#define _40_SYS_BATL_LED_BIT 0x40
#define _80_SYS_BATN_LED_BIT 0x80
#define _40_SYS_AC_LED_BIT 0x40
#define _C0_SYS_BATOFF_LED_BIT 0xC0
 
typedef enum
{
	LED_NORMAL_STATE = 0,
	LED_TEST_STATE,
}LED_STATE_T;



LED_STATE_T LED_state = LED_NORMAL_STATE;
uint8_t u8_G_led_test_arr[NO_OF_ROWS]={0x00};
uint8_t u8Ledcnt = 0,u8LedTestTime = 0;

uint16_t LED_dat = 0x00;
uint8_t EXT_devDat = 0x00;

uint8_t u8_G_led_data_arr[NO_OF_ROWS]={0x00};
uint8_t u8_G_led_type_arr[NO_OF_ROWS]={0xff};
uint8_t u8_G_led_toggle_arr[NO_OF_ROWS]={0x00}; 
G_RESP_CODE h_vLED_shiftout(void);

 

 void LED_create(void)
 {	 
	 for (size_t i = 0; i < NO_OF_ROWS; i++)
	 {
		 u8_G_led_data_arr[i] = 0;
		 h_LED_SerialSend(~(u8_G_led_data_arr[i] ^ u8_G_led_type_arr[i]));
	 }	
 }
 

G_RESP_CODE h_vLED_shiftout(void)                  
{
    G_RESP_CODE ret = SUCCESS;
	 h_LED_SerialSend(~(u8_G_led_data_arr[0]^u8_G_led_type_arr[0]));      
     return ret;
} 
void m_LED_ctrl(LED_NAME name,G_CTL_CODES cmd)
{
	uint8_t index = 0,bit_pos = 0;
	index = name/8;
	bit_pos = name%8;
	if(cmd == ON)
	{
		u8_G_led_data_arr[index] = u8_G_led_data_arr[index] | (uint8_t)(0x01 << bit_pos);
		u8_G_led_toggle_arr[index] = u8_G_led_toggle_arr[index] & (uint8_t)(~(0x01 << bit_pos));
	}
	else if(cmd == OFF)
	{
		u8_G_led_data_arr[index] = u8_G_led_data_arr[index] & (uint8_t)(~(0x01 << bit_pos));
		u8_G_led_toggle_arr[index] = u8_G_led_toggle_arr[index] & (uint8_t)(~(0x01 << bit_pos));
	}
	else if(cmd == TOGGLE)
	{
		u8_G_led_data_arr[index] = u8_G_led_data_arr[index] & (uint8_t)(~(0x01 << bit_pos));
		u8_G_led_toggle_arr[index] = u8_G_led_toggle_arr[index] | (uint8_t)(0x01 << bit_pos);		
	}
	else if(cmd == RESET)
	{
		u8_G_led_data_arr[0] = 0x00;
		u8_G_led_data_arr[1] = 0x00;
		u8_G_led_toggle_arr[0] = 0x00;
		u8_G_led_toggle_arr[1] = 0x00;
	}	
}

void m_u8LED_ctrl_led(LED_NAME choice,G_CTL_CODES cmd)
{	 
	m_LED_ctrl(choice,cmd);
	h_vLED_shiftout();
	LED_state = LED_NORMAL_STATE;
}

