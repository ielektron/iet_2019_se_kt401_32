import os
import sys

f = open("compile.cmd",'w+');
def compile_test_case_cpp(cpp):
    cmd_file_contents = """set mypath=%cd%
for %%I in (.) do set ProjName=%%~nxI
set ProjName={cppfile}
g++ "-I%mypath%\GTST" "-I%mypath%\INC" -O0 -g3 -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "DBG\TST\gtst_main.o" "%mypath%\TST\gtst_main.cpp"
g++ "-I%mypath%\GTST" "-I%mypath%\INC" -O0 -g3 -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "DBG\TST\{cppfile}.o" "%mypath%\TST\{cppfile}.cpp"
g++ "-I%mypath%\GTST" "-I%mypath%\INC" -O0 -g3 -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "DBG\GTST\gtest\gtest-all.o" "%mypath%\GTST\gtest\gtest-all.cc"
g++ -ftest-coverage -fprofile-arcs -o %ProjName% "DBG\GTST\gtest\gtest-all.o"  "DBG\TST\gtst_main.o" "DBG\TST\{cppfile}.o" -lpthread
%ProjName%.exe --gtest_output=xml:RPT\RESULTS\%ProjName%_results.xml > TST\{cppfile}_result.txt
gcov -o "%mypath%\DBG\TST\gtst_main.gcno" "%mypath%\TST\gtst_main.cpp"
gcov -o "%mypath%\DBG\TST\{cppfile}.gcno" "%mypath%\TST\{cppfile}.cpp"
echo "test_completed:{cppfile}"
"""
    f.write(cmd_file_contents.format(cppfile = cpp));

def get_complete_coverage(cpp):
    cmd_file_contents = """gcovr -r "%mypath%" > cover.txt
cd "%mypath%\RPT\COVERAGE"
gcovr -r "%mypath%" --html --html-details -o coverage.html
cd "%mypath%"
echo "static code analysis starts";
cppcheck INC\ 2> analysis.txt
python test_results.py
echo "all_test_completed"
pause
"""
    f.write(cmd_file_contents.format(cppfile = cpp));

cfiles = [];
cppfiles = [];

directory = os.listdir(os.getcwd()+"\TST");
cppfiles = [];

#print(directory);
for files in directory:
    #print(files[len(files)-3:len(files)-1]);
    if(files[len(files)-3:len(files)]=="cpp"):
        file = files[0:len(files)-4];
        cppfiles.append(file);


for file in cppfiles:
    if(file != "gtst_main"):
        compile_test_case_cpp(file);

get_complete_coverage(file);

f.close()
print("ok")

    
    
