#include "M_led_handler.h" 
#include "r_cg_serial.h"
#include "PIN_CFG.h"
 
#define LED_TEST_INT 20
#define HIGH 1
#define LOW 0

#define NO_OF_ROWS 1
#define NO_OF_COLUMNS 8

#define _80_SYS_ALT_LED_BIT 0x80
#define _40_SYS_MUTE_LED_BIT 0x40
#define _80_SYS_NIGHT_LED_BIT 0x80
#define _40_SYS_DAY_LED_BIT 0x40
#define _40_SYS_BATL_LED_BIT 0x40
#define _80_SYS_BATN_LED_BIT 0x80
#define _40_SYS_AC_LED_BIT 0x40
#define _C0_SYS_BATOFF_LED_BIT 0xC0
 
typedef enum
{
	LED_NORMAL_STATE = 0,
	LED_TEST_STATE,
}LED_STATE_T;



LED_STATE_T LED_state = LED_NORMAL_STATE;
uint8_t u8_G_led_test_arr[NO_OF_ROWS]={0x00};
uint8_t u8Ledcnt = 0,u8LedTestTime = 0;

uint16_t LED_dat = 0x00;
uint8_t EXT_devDat = 0x00;

uint8_t u8_G_led_data_arr[NO_OF_ROWS]={0x00};
uint8_t u8_G_led_type_arr[NO_OF_ROWS]={0xff};
uint8_t u8_G_led_toggle_arr[NO_OF_ROWS]={0x00};
void LED_test(void);
G_RESP_CODE h_vLED_shiftout(void);
void EXT_devShiftout(void);
void LED_TestStart(void)
{
	u8Ledcnt = 0;
	u8LedTestTime = 0;
	u8_G_led_test_arr[0] = 0x00;
	u8_G_led_test_arr[1] = 0x00;
	LED_dat = 0;
	LED_state = LED_TEST_STATE;
}
 void h_LED_hdlr(void)
 {
	 switch(LED_state)
	 {
		 case LED_NORMAL_STATE:
		 {
			 u8_G_led_data_arr[0] = u8_G_led_data_arr[0] ^ u8_G_led_toggle_arr[0];
     			 u8_G_led_data_arr[1] = u8_G_led_data_arr[1] ^ u8_G_led_toggle_arr[1];
			 h_vLED_shiftout();
			 break;
		 }		 
		 case LED_TEST_STATE:
		 {
			
			/* u8_G_led_test_arr[u8Ledcnt/8] = u8_G_led_test_arr[u8Ledcnt/8]<<(u8Ledcnt%8);*/	
			 
			LED_dat =  0x0001<<u8Ledcnt;
			 u8Ledcnt++;
			 LED_test();
			 
			 if(u8Ledcnt >= 15)
			 {
				u8Ledcnt = 0; 
			 }
			 u8LedTestTime++;
			 if(u8LedTestTime>LED_TEST_INT)
			 {
				 u8LedTestTime = 0;
				 LED_state = LED_NORMAL_STATE;
			 }
			 break;
		 }
		 default:
		 break;
	 }
 }

void LED_test(void)
{
     int8_t column;      
     LED_SHIFT_LATCH = LOW;     
     for (column = NO_OF_COLUMNS-1; column >=0; column--)  
     {	   
           /*LED_SHIFT1_DIN = (((~(u8_G_led_test_arr[0]^u8_G_led_type_arr[0])) & (1 << column))?HIGH:LOW);
	   LED_SHIFT2_DIN = (((~(u8_G_led_test_arr[1]^u8_G_led_type_arr[1])) & (1 << column))?HIGH:LOW);*/
	   
	   //LED_SHIFT1_DIN =  (LED_dat & (0x0001<<column))?HIGH:LOW;
	   //LED_SHIFT2_DIN =  (LED_dat & (0x0001<<(column+8)))?HIGH:LOW;
	   
	   LED_SHIFT1_DIN =  (LED_dat>>column) & 0x0001;
	   //LED_SHIFT2_DIN =  (LED_dat>>(column+8)) & 0x0001;
	    
	   LED_SHIFT_CLOCK = HIGH;   
	   LED_SHIFT_CLOCK = LOW;   
     }    
     LED_SHIFT_LATCH = HIGH;
}

G_RESP_CODE h_vLED_shiftout(void)                  
{
     int8_t column;
     uint8_t ret;
     LED_SHIFT_LATCH = LOW;     
     for (column = NO_OF_COLUMNS-1; column >=0; column--)  
     {	   
           LED_SHIFT1_DIN = (((~(u8_G_led_data_arr[0]^u8_G_led_type_arr[0])) & (1 << column))?HIGH:LOW);	   
	   LED_SHIFT_CLOCK = HIGH;   
	   LED_SHIFT_CLOCK = LOW;   
     }
     if(column == NO_OF_COLUMNS)
     {
	   ret = SUCCESS;     
     }
     LED_SHIFT_LATCH = HIGH;
     return ret;
} 
void m_LED_ctrl(LED_NAME name,G_CTL_CODES cmd)
{
	uint8_t index = 0,bit_pos = 0;
	index = name/8;
	bit_pos = name%8;
	if(cmd == ON)
	{
		u8_G_led_data_arr[index] = u8_G_led_data_arr[index] | (uint8_t)(0x01 << bit_pos);
		u8_G_led_toggle_arr[index] = u8_G_led_toggle_arr[index] & (uint8_t)(~(0x01 << bit_pos));
	}
	else if(cmd == OFF)
	{
		u8_G_led_data_arr[index] = u8_G_led_data_arr[index] & (uint8_t)(~(0x01 << bit_pos));
		u8_G_led_toggle_arr[index] = u8_G_led_toggle_arr[index] & (uint8_t)(~(0x01 << bit_pos));
	}
	else if(cmd == TOGGLE)
	{
		u8_G_led_data_arr[index] = u8_G_led_data_arr[index] & (uint8_t)(~(0x01 << bit_pos));
		u8_G_led_toggle_arr[index] = u8_G_led_toggle_arr[index] | (uint8_t)(0x01 << bit_pos);		
	}
	else if(cmd == RESET)
	{
		u8_G_led_data_arr[0] = 0x00;
		u8_G_led_data_arr[1] = 0x00;
		u8_G_led_toggle_arr[0] = 0x00;
		u8_G_led_toggle_arr[1] = 0x00;
	}	
}

void m_u8LED_ctrl_led(LED_NAME choice,G_CTL_CODES cmd)
{
	/*
	uint8_t bit_index = choice%10;
	uint8_t ic_index = (choice-bit_index)/10;
	ic_index = ic_index-1;
	if((ic_index < 3)&&(bit_index<8))
	{
		if(cmd == ON)
		{
		u8_G_led_data_arr[ic_index] = u8_G_led_data_arr[ic_index] | ((uint8_t)1 << bit_index);
		}
		else if(cmd == OFF)
		{
		u8_G_led_data_arr[ic_index] = u8_G_led_data_arr[ic_index] & (~((uint8_t)1 << bit_index));	
		}
		else
		{
			
		}
	}*/
	m_LED_ctrl(choice,cmd);
	h_vLED_shiftout();
	LED_state = LED_NORMAL_STATE;
}

