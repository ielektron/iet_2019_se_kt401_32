#ifndef STUB_H
#define STUB_H

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_macrodriver.h"
#include "r_cg_userdefine.h"
#include "M_led_handler.h"

/***********************************************************************************************************************
* Function Name: get_led_name
* Description  : This function returns the led info which will be used by the test case.
* Arguments    : None
* Return Value : one value of enum LED_NAME
***********************************************************************************************************************/
LED_NAME get_led_name(void);

/***********************************************************************************************************************
* Function Name: get_led_sts
* Description  : This function returns the led info which will be used by the test case.
* Arguments    : None
* Return Value : one value of enum G_CTL_CODES
***********************************************************************************************************************/

G_CTL_CODES get_led_sts(void);

#endif

