#pragma mul

#include "M_ZONE_fire_hdlr.h"
#include "M_ADC_hdlr.h" 
#include "H_lcd_driver.h"
#include "M_led_handler.h"
#include "PIN_CFG.h"
#include "M_power_handler.h"
#include "M_ZONE_intr_hdlr.h"
#include "M_GeneralLib.h"
#include "A_UI_handler.h"
#include "M_eeprom_handler.h"
 


#define ZONE_OPEN_TH 680
#define ZONE_NORMAL_TH 590
#define ZONE_ALERT_TH 325
#define ZONE_SHORT_TH 50
#define ZONE_TH 20
#define ZONE_TH2 160




#define RESTART_WAIT_INTERVAL 20
#define START_WAIT_INTERVAL 10
#define WALK_TEST_TIMEOUT 120//10MIN
#define WALK_TEST_ALT_TH 10//5 seconds
#define WALK_TEST_SIL_TH 16//5+3 = 8 seconds




typedef enum
{
	ZONE_MONITOR = 0,
	ZONE_TEST,
	ZONE_TEST_ALT,
	ZONE_FIRE_ALT,
	ZONE_RESTART,
	ZONE_START_WAIT,
	ZONE_DISABLE_STATE,
	ZONE_POW_SHUT,
}ZONE_STATE_T;

typedef struct
{
	ZONE_STATE_T ZONE_state;
	ZONE_STATUS_T ZONE_status;	 
	uint16_t ZONE_cnt;
	ZONE_MODE_T ZONE_mode;
}ZONE_SETTING_ST;

ZONE_SETTING_ST ZONE_setting_st[NO_OF_FIRE_ZONES] = {{ZONE_MONITOR,ZONE_NORMAL,0}};



const uint8_t FIRE_sts[ZONE_STS_END][4]={"ALT","DIS ","OP ","SH ","RES ","NORM","WT "};
 
void ZONE_FIRE_NotiHdlr(ZONE_NAME_T name,ZONE_STATUS_T status);
ZONE_STATUS_T ZONE_stsGet(uint8_t zone_index);
void ZONE_FIRE_monitor(void)
{
 	uint8_t i = 0;
	uint16_t buffer = 0;
	
	uint8_t status;
	
	
	for(i = 0;i<NO_OF_FIRE_ZONES;i++)
	{
		switch(ZONE_setting_st[i].ZONE_state)
		{
			case ZONE_MONITOR:
			{
				status = ZONE_stsGet(i);				 
				if(ZONE_setting_st[i].ZONE_status-status)
				{
					ZONE_setting_st[i].ZONE_status = status;
					//ZONE_setting_st[i].ZONE_status = ZONE_ALT;
					if(ZONE_setting_st[i].ZONE_status == ZONE_ALT)
					{						 
						APP_eventManager(E_ALT_TRIG,DEFAULT);
						E2P_LOG_STORE();
					}
					else
					{
						ZONE_FIRE_NotiHdlr(i,ZONE_setting_st[i].ZONE_status);
					}
				}
				break;
			}			 
			case ZONE_RESTART:
			{
				ZONE_setting_st[i].ZONE_cnt++;
				if(ZONE_setting_st[i].ZONE_cnt > RESTART_WAIT_INTERVAL)
				{
					ZONE_setting_st[i].ZONE_cnt = 0;
					M_ZONE_control(i,ON);					 
					ZONE_setting_st[i].ZONE_state = ZONE_START_WAIT;
				}
				break;
			}
			case ZONE_START_WAIT:
			{
				ZONE_setting_st[i].ZONE_cnt++;
				if(ZONE_setting_st[i].ZONE_cnt > START_WAIT_INTERVAL)
				{
					ZONE_setting_st[i].ZONE_cnt = 0;
					ZONE_setting_st[i].ZONE_state = ZONE_MONITOR;
					ZONE_setting_st[i].ZONE_status = ZONE_NORMAL;					 					 	
					//NOTI_hdlr(i,ZONE_setting_st[i].ZONE_status);
				}
				break;
			}
			case ZONE_DISABLE_STATE:
			{
				break;
			}
			case ZONE_POW_SHUT:
			{
				break;
			}
			default:
			break;
		}
	}
	
	
	
	
}
void M_ZONE_control(ZONE_NAME_T zone_no,G_CTL_CODES ctl)
{
	switch(zone_no)
	{
		case ZONE_1_FIRE:
		{
			ZONE_1_FIRE_ENABLE = ctl;
			break;
		}
		case ZONE_2_FIRE:
		{
			ZONE_2_FIRE_ENABLE = ctl;
			break;
		}
		default:
		break;
	}
}

void ZONE_FIRE_reset(void)
{
	uint8_t i = 0;	 
	for(i = 0;i<NO_OF_FIRE_ZONES;i++)
	{
		if(ZONE_setting_st[i].ZONE_mode != ISOLATE_MODE)
		{
			M_ZONE_control(i,OFF);  
			ZONE_setting_st[i].ZONE_state = ZONE_RESTART;
			ZONE_setting_st[i].ZONE_cnt = 0;	
			ZONE_setting_st[i].ZONE_status = ZONE_RESET;
			//NOTI_hdlr(i,ZONE_setting_st[i].ZONE_status);
		}			 
	}	
} 
void ZONE_FIRE_init(void)
{
	uint8_t i = 0;
	for(i = 0;i<NO_OF_FIRE_ZONES;i++)
	{		   			 
		ZONE_setting_st[i].ZONE_state = ZONE_START_WAIT;
		ZONE_setting_st[i].ZONE_cnt = 0;
		ZONE_setting_st[i].ZONE_status = ZONE_RESET;	
		M_ZONE_control(i,ON); 		 
		if(ZONE_setting_st[i].ZONE_mode == ISOLATE_MODE)
		{
			ZONE_setting_st[i].ZONE_state = ZONE_DISABLE_STATE;
			ZONE_setting_st[i].ZONE_status = ZONE_DISABLE;
			M_ZONE_control(i,OFF);
		}			 
		//NOTI_hdlr(i,ZONE_setting_st[i].ZONE_status); 		 		 		 
	}
}   
 
ZONE_STATUS_T ZONE_stsGet(uint8_t zone_index)
{
	ZONE_STATUS_T status = ZONE_setting_st[zone_index].ZONE_status;
	uint16_t buffer = 0;
	if(ADC_STABLE == M_ADC_CHsts_get(zone_index+ADC_FIRE_OFFSET,&buffer))
	{
		if((ZONE_OPEN_TH-ZONE_TH) <= buffer)
		{
			status = ZONE_OPEN;	 
		}
		else if(((ZONE_NORMAL_TH-ZONE_TH) <= buffer) && (((ZONE_NORMAL_TH+ZONE_TH) > buffer)))
		{
			status = ZONE_NORMAL;
		}
		else if(((ZONE_ALERT_TH-ZONE_TH2) <= buffer) && (((ZONE_ALERT_TH+ZONE_TH) > buffer)))
		{
			status = ZONE_ALT;						
		}
		else if((ZONE_SHORT_TH+ZONE_TH) > buffer)
		{
			status = ZONE_SHORT;						
		}	 
	}
	return status;
} 
void ZONE_FIRE_EventManager(E_SYS_EVENTS event,uint8_t sts)
{
	uint8_t i = 0;
	if(event == E_POWER)
	{
		switch(sts)
		{
			case BAT_SHUT:
			{
				for(i = 0;i<NO_OF_FIRE_ZONES;i++)
				{
					if(ZONE_setting_st[i].ZONE_mode != ISOLATE_MODE)
					{
						ZONE_setting_st[i].ZONE_state = ZONE_POW_SHUT;					 
					}
				}
				break;
			}
			case POW_AC:
			{
				for(i = 0;i<NO_OF_FIRE_ZONES;i++)
				{
					if(ZONE_setting_st[i].ZONE_state == ZONE_POW_SHUT)
					{
						ZONE_setting_st[i].ZONE_cnt = 0;
						if(ZONE_setting_st[i].ZONE_status == ZONE_RESET)
						{
							M_ZONE_control(i,OFF);
							ZONE_setting_st[i].ZONE_state = ZONE_RESTART;
						}
						else
						{
							if(ZONE_setting_st[i].ZONE_mode != ISOLATE_MODE)
							{						
								ZONE_setting_st[i].ZONE_state = ZONE_MONITOR;
								ZONE_setting_st[i].ZONE_status = ZONE_NORMAL;							 											 
							}
						}
						//NOTI_hdlr(i,ZONE_setting_st[i].ZONE_status); 
					}				
				}
				break;
			}
			default:
			break;	
		}
	}	
}
uint8_t* ZONE_FIRE_LogStore(uint8_t *arr)
{
	uint8_t zone=0,k = 0;
	
    	for(zone = 0;zone < NO_OF_FIRE_ZONES;zone++)
    	{
	    k =  u8GL_ncopy(arr,(uint8_t *)"ZF",2);
    	    arr = arr + k;
    	    *arr = '=';
    	    arr++;
	    if(ZONE_setting_st[zone].ZONE_status < ZONE_STS_END)
	    {
	      	k =  u8GL_ncopy(arr,FIRE_sts[ZONE_setting_st[zone].ZONE_status],4);
	    }
	    else
	    {
	    	k =  u8GL_ncopy(arr,(uint8_t *)"ERR",4); 
	    }
	    arr = arr + k;	    
    	    *arr = ZONE_DELIMITER;
    	    arr++;
    	}
	
	k =  u8GL_ncopy(arr,(uint8_t *)"T ",2);
    	arr = arr + k;
    	*arr = '=';
    	arr++;
    	if(tamper_sts < ZONE_TRIG_END)
    	{
		if(tamper_sts == TAMP_DEFAULT)
		{
			k = u8GL_ncopy(arr,FIRE_sts[0],4);
		}
		else
		{
			k = u8GL_ncopy(arr,FIRE_sts[5],4);
		}    	   	
    	}
    	else
    	{
	    	k =  u8GL_ncopy(arr,(uint8_t *)"ERR",4); 
    	}
    	arr = arr + k;
    	*arr = ZONE_DELIMITER;
    	arr++;    	 
    	return arr;
}
void ZONE_FIRE_NotiHdlr(ZONE_NAME_T name,ZONE_STATUS_T status)
{
	uint8_t result = 0;
	
	if((status == ZONE_OPEN) || (status == ZONE_SHORT))
	{
		result = (uint8_t)(mulu(name,10) + status);	
		Event_store(result-1,EVENT_REPLACE);
	}
	else
	{
		result = (uint8_t)(mulu(name,10) + status);	
		Event_store(result-1,EVENT_DELETE);
	}
}
ZONE_STATUS_T ZONE_FIRE_stsGet(void)
{
	return ZONE_setting_st[0].ZONE_status;
}