#ifndef M_ZONE_HDLR_H
#define M_ZONE_HDLR_H
#include "r_cg_macrodriver.h"
#include "r_cg_userdefine.h"
#include "A_main_app_handler.h" 

#define NO_OF_FIRE_ZONES 1
#define NO_OF_ZONES NO_OF_FIRE_ZONES
 

typedef enum
{
	ZONE_1_FIRE,
	ZONE_2_FIRE
}ZONE_NAME_T;
typedef enum
{
	NORMAL_MODE = 0,
	WALK_TEST_MODE,
	ISOLATE_MODE,
	ZONE_MODE_LEN
}ZONE_MODE_T;
typedef enum
{
	ZONE_ALT = 0,
	ZONE_DISABLE,
	ZONE_OPEN,
	ZONE_SHORT,
	ZONE_RESET,
	ZONE_NORMAL,
	ZONE_WT,	 
	ZONE_STS_END
}ZONE_STATUS_T;
 
extern uint8_t tamper_sts;

void ZONE_FIRE_EventManager(E_SYS_EVENTS event,uint8_t sts); 
void M_ZONE_control(ZONE_NAME_T zone_no,G_CTL_CODES ctl); 
void ZONE_FIRE_reset(void); 
void ZONE_FIRE_monitor(void); 
void ZONE_FIRE_init(void);
uint8_t* ZONE_FIRE_LogStore(uint8_t *arr);
ZONE_STATUS_T ZONE_FIRE_stsGet(void);
#endif