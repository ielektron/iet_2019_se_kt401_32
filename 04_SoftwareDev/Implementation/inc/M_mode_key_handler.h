#ifndef M_MODE_KEY_HANDLER_H
#define M_MODE_KEY_HANDLER_H

#include "r_cg_macrodriver.h"
#include "r_cg_userdefine.h"

//PIN DEFNITIONS
#define SYS_MODE_TYPE 3

typedef enum
{
	EXT_KEY = 0,
	UI_KEY = 1
}MODE_KEY_TYPE;
 
//dont change the order-see s8_G_DNB[] in zone handler
typedef enum
{
	DISARM_MODE = 0,
	PART_ARM_MODE,	
	ARM_MODE,	 	 
	DEFAULT_MODE=4,//it must not be less than 4 because if the key is not connected the value read by gpio is 3
	SYS_MODE_END
}SYS_MODE_E;
 


SYS_MODE_E m_vMKEY_monitor(void);
G_RESP_CODE m_u8ModeHdlr_update(const uint8_t *dat);
SYS_MODE_E m_u8MKEY_sts_get(void);
void m_u8MKEY_sts_set(SYS_MODE_E mode);
MODE_KEY_TYPE m_u8MKEY_type_get(void);
#endif
