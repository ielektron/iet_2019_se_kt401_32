/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by IELEKTRON TECHNOLOGIES
* applicable laws, including copyright laws. 
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIESREGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED
* OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY
* LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR
* ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
*
* Copyright (C) 2017, ielektron technologies. All rights reserved.
***********************************************************************************************************************/

/***********************************************************************************************************************
* File Name    : i_delay.c
* Version      : CodeGenerator for RL78/G13 V2.03.02.01 [15 May 2015]
* Device(s)    : R5F100FE
* Tool-Chain   : CA78K0R
* Description  : This file implements main function.
* Creation Date: 8/25/2017
***********************************************************************************************************************/
#include "r_cg_macrodriver.h"
 
#include "H_delay_driver.h"
#include "r_cg_timer.h"

extern volatile uint16_t u16delay_tick_us,u16delay_tick_ms;
extern volatile uint32_t tick_micro;

void DelayMs(uint8_t del_val)
{
 uint32_t i=0;
 for(i=0;i<1000*del_val;i++);
}

void DelayUs(uint16_t del_val)
{
 /*uint32_t i=0;
 for(i=0;i<100*del_val;i++);*/
 R_TAU0_Channel1_Start();
 while(u16delay_tick_us < del_val);
 u16delay_tick_us = 0;
 R_TAU0_Channel1_Stop();
}
 
void h_vDEL_Ms(uint16_t del_val)
{
	 R_TAU0_Channel3_Start();
	 while(u16delay_tick_ms < del_val);
	 u16delay_tick_ms = 0;
	 R_TAU0_Channel3_Stop();
}

void h_vDEL_100nano(uint32_t val)
{
	 R_TAU0_Channel2_Start();
	 while(tick_micro<val);
	 tick_micro = 0;
	 R_TAU0_Channel2_Stop();
}