#pragma mul

#include "M_ZONE_intr_hdlr.h" 
#include "M_mode_key_handler.h"
#include "r_cg_serial.h"
#include "stdlib.h"
#include "string.h"
#include "stdio.h"
#include "M_led_handler.h"
#include "A_main_app_handler.h"
#include "M_eeprom_handler.h"
#include "M_event_handler.h"
#include "M_GeneralLib.h"
#include "A_UI_handler.h"
#include "PIN_CFG.h" 
#include "M_ZONE_fire_hdlr.h"





#define MAX_DEBOUNCE 2
#define MAX_DELAY_TIME 99

#define ZO_ZC_MAX_INDEX 4
#define ZO_ZC_STR_LEN 5
#define ZONE_SELECT_STR_LEN 4
#define DNB_STR_LEN 4


uint8_t tamper_sts = ZONE_TRIG_END;

ZONE_SETTINGS sensor_index[ZONE_COUNT] = {
					  {ACTIVATE,DEFAULT_TRIG,0,ARM_MODE,NC,0,NO_ALT,0},{ACTIVATE,DEFAULT_TRIG,0,ARM_MODE,NC,0,NO_ALT,0},
					  {ACTIVATE,DEFAULT_TRIG,0,ARM_MODE,NC,0,NO_ALT,0},{ACTIVATE,DEFAULT_TRIG,0,DISARM_MODE,NC,0,NO_ALT,0}
					 };			
 
const uint8_t s8_G_zone_select[ZONE_COUNT][ZONE_SELECT_STR_LEN] = {"Z1","Z2","Z3","Z4"}; 
 
const uint8_t s8_G_ZO_ZC[ZO_ZC_MAX_INDEX][ZO_ZC_STR_LEN]={"NORM","ALT ","ALT ","NW  "};
uint8_t ZONE_stsArray[ZONE_COUNT+NO_OF_FIRE_ZONES] = {0};

G_RESP_CODE m_vZONE_monitor(void)
{	 
	G_RESP_CODE ret = WAITING;
	uint8_t tamper = 0;
	uint8_t zone=0,alt_flag = 0;
	uint16_t u16_val=0;
	sensor_index[0].zone_input = ZONE1_IN;
	sensor_index[1].zone_input = ZONE2_IN;
	sensor_index[2].zone_input = ZONE3_IN;
	sensor_index[3].zone_input = ZONE4_IN;
	tamper = TAMPER;
	
	for(zone = 0; zone < ZONE_COUNT;zone++)
	{
		switch(sensor_index[zone].zone_sts)
		{
			case NO_ALT:
			{
				if(sensor_index[zone].active_sts==ACTIVATE)
				{
					if(( m_u8MKEY_sts_get() >= sensor_index[zone].mode_setting)&&(sensor_index[zone].trig_setting==sensor_index[zone].zone_input))
					{
						sensor_index[zone].db_cnt++;
						if(sensor_index[zone].db_cnt > MAX_DEBOUNCE)
						{
							sensor_index[zone].zone_sts=ALT_SENSED;
							sensor_index[zone].db_cnt = 0;
							alt_flag  =1;
						}	
					}
					else
					{
						sensor_index[zone].db_cnt = 0;
					}
				}
				break;
			}
			case ALT_SENSED:
			{
				sensor_index[zone].zone_dly_cnt++;	
				u16_val =(uint16_t) mulu((uint8_t)sensor_index[zone].delay_timing,2);
				if(sensor_index[zone].zone_dly_cnt > u16_val)
				{					
					sensor_index[zone].zone_dly_cnt = 0;				 
					sensor_index[zone].zone_sts=ALT_TRIGGERED;					 
					APP_eventManager(E_ALT_TRIG,DEFAULT);
				}
				break;
			}
			case NIGHT_WAIT:
			{
				sensor_index[zone].zone_dly_cnt++;
				u16_val = (uint16_t)mulu((uint8_t)sensor_index[zone].delay_timing,2);
				if(sensor_index[zone].zone_dly_cnt > u16_val)
				{
					sensor_index[zone].zone_sts = NO_ALT;
					sensor_index[zone].zone_dly_cnt=0;
				}
				break;
			}
			default:
			break;
		}
	}	
	if(tamper_sts != tamper)
	{ 
		tamper_sts = tamper;
		if(tamper == TAMP_DEFAULT)
		{		   
		  APP_eventManager(E_ALT_TRIG,DEFAULT);
		  E2P_LOG_STORE();
		}
	}
	if(alt_flag == 1)
	{		 
		E2P_LOG_STORE();
	}
	return ret;
}

void m_u8ZONE_reset(void)
{	
	uint8_t zone=0;
	SYS_MODE_E cmd;
	cmd = m_u8MKEY_sts_get();
	for(zone = 0; zone < ZONE_COUNT;zone++)
	{
		sensor_index[zone].zone_dly_cnt = 0;		
		if(DISARM_MODE == cmd)  
		{
			sensor_index[zone].zone_sts = NO_ALT;
						
		}
		else
		{
			//ch-27-10
			if((ACTIVATE == sensor_index[zone].active_sts)&&(cmd >= sensor_index[zone].mode_setting)
								&&(sensor_index[zone].mode_setting != DISARM_MODE))
			{
				sensor_index[zone].zone_sts = NIGHT_WAIT;
			}	
			else
			{
				sensor_index[zone].zone_sts = NO_ALT;
			}
		}		 
	}	 
}
uint8_t* ZONE_LogStore(uint8_t *arr)
{
    uint8_t zone=0;
    char stri[MAX_DIGITS+1]={0};	 
    uint8_t k =0; 
    k = m_u8MKEY_sts_get();
    if(k >= SYS_MODE_TYPE)
    {
	k = DEFAULT_MODE-1;//refer SYS_MODE_E
    }
    k =  u8GL_ncopy(arr,(uint8_t*)s8_G_arm_parm[k],MODE_STR_LEN-1);
    arr = arr + k;
   *arr = LINE_DELIMITER;
    arr++;
   /* 
    sensor_index[0].active_sts = ISOLATE;
    sensor_index[0].trig_setting = NC;
    sensor_index[0].delay_timing = 50;
    sensor_index[0].mode_setting = PART_ARM_MODE;
    sensor_index[0].zone_sts = NIGHT_WAIT;
    
    sensor_index[1].active_sts = ISOLATE;
    sensor_index[1].trig_setting = 3;
    sensor_index[1].delay_timing = 99;
    sensor_index[1].mode_setting = DISARM_MODE;
    sensor_index[1].zone_sts = NO_ALT;
    
    sensor_index[2].active_sts = ACTIVATE;
    sensor_index[2].trig_setting = 4;
    sensor_index[2].delay_timing = 0;
    sensor_index[2].mode_setting = ARM_MODE;
    sensor_index[2].zone_sts = ALT_SENSED;
    
    sensor_index[3].active_sts = 5;
    sensor_index[3].trig_setting = NO;
    sensor_index[3].delay_timing = 120;
    sensor_index[3].mode_setting = DEFAULT_MODE;
    sensor_index[3].zone_sts = ALT_TRIGGERED;
    */
    for(zone = 0;zone<ZONE_COUNT;zone++)
    {   
	    
       k =  u8GL_ncopy(arr,s8_G_zone_select[zone],ZONE_SELECT_STR_LEN-1);
       arr = arr + k;
       *arr = '=';
       arr++;
       if(sensor_index[zone].active_sts < ACTIVE_STS_END)
       {
	       k =  u8GL_ncopy(arr,(uint8_t*)s8_G_active_isolate[sensor_index[zone].active_sts],1);
	       arr = arr + k;	        
       }
       else
       {
	       k =  u8GL_ncopy(arr,(uint8_t *)"E",1);
	       arr = arr + k;
       }
       *arr = STS_DELAY_DELIMITER;
	arr++; 
       if(sensor_index[zone].trig_setting < ZONE_TRIG_END)
       {
	       k =  u8GL_ncopy(arr,(uint8_t*)s8_G_NO_NC[sensor_index[zone].trig_setting],2);
	       arr = arr + k;	       
       }
       else
       {
	       k =  u8GL_ncopy(arr,(uint8_t *)"E",1);
	       arr = arr + k;
       }
       *arr = STS_DELAY_DELIMITER;
	arr++; 
       if(sensor_index[zone].delay_timing <= MAX_DELAY_TIME)
       {
       	     u8GL_num2str(sensor_index[zone].delay_timing, stri);
	     k = u8GL_ncopy(arr,(uint8_t*)stri,MAX_DIGITS);
	     arr = arr + k;	     
       }
       else
       {
	       k =  u8GL_ncopy(arr,(uint8_t *)"E",1);
	       arr = arr + k;
       }
       *arr = STS_DELAY_DELIMITER;
	arr++; 
       if(sensor_index[zone].mode_setting < SYS_MODE_TYPE)
       {
	       k =  u8GL_ncopy(arr,(uint8_t*)s8_G_arm_parm[sensor_index[zone].mode_setting],MODE_STR_LEN);
	       arr = arr + k;	        
       }
       else
       {
	       k =  u8GL_ncopy(arr,(uint8_t *)"E",1);
	       arr = arr + k;
       }
       *arr = STS_DELAY_DELIMITER;
	arr++; 
       if(sensor_index[zone].zone_sts < ZO_ZC_MAX_INDEX)
       {
       	       k =  u8GL_ncopy(arr,s8_G_ZO_ZC[sensor_index[zone].zone_sts],ZO_ZC_STR_LEN-1);
	       arr = arr + k;        
       }
       else
       {
	       k =  u8GL_ncopy(arr,(uint8_t *)"E",1);
	       arr = arr + k;	       
       }  
       *arr = LINE_DELIMITER;
	arr++; 
       /*if(zone == (ZONE_COUNT-1))
       {
       	*arr = ZONE_DELIMITER;
       	 arr++;    
       }
       else
       {
	 *arr = STS_DELAY_DELIMITER;
       	 arr++;  
       }*/     
    }   	
    return arr;	
}
void ZONE_StsStore(uint8_t *arr,E_SYS_EVENTS command)
{	uint8_t zone=0;
	arr = arr + ZONE_STS_INDEX;
	for(zone = 0;zone<ZONE_COUNT;zone++)
    	{
	    *(arr + zone) = sensor_index[zone].zone_sts;
    	}
	*(arr + zone) = ZONE_FIRE_stsGet();	
	*(arr + zone + 1) = tamper_sts;
	*(arr + zone + 2) = command;
	
	
}
 