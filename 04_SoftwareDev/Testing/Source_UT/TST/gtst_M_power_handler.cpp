
/*

filename : gtst_power_handler.cpp
author : rudresh
created at : 2019-04-05 19:06:49.602985

*/
    
# include "gtest/gtest.h"
//#include "pch.h"
extern "C"
{	 
	#include "M_power_handler.h"	
    #include "M_power_handler.c"	
	#include "PowerHandlerStub.h"
	#include "PowerHandlerStub.c"
}

class POWER_HANDLER_TEST : public::testing::Test {
protected:
	POW_MODE_T PowerSts, BatterySts;
	void SetUp()
	{
		PowerHandlerCreate();
	}
};


//RequirementTest PositiveTestCase
TEST_F(POWER_HANDLER_TEST, RequirementTest1)
{	
	STB_ADC_set(500, 800);
	m_POW_monitor();
	Get_status(&PowerSts,&BatterySts);
	ASSERT_EQ(POW_AC, PowerSts);
}

//RequirementTest PositiveTestCase

TEST_F(POWER_HANDLER_TEST, RequirementTest2)
{ 
	STB_ADC_set(400,800);
	m_POW_monitor();
	Get_status(&PowerSts, &BatterySts);
	ASSERT_EQ(BAT_NORM, PowerSts);
}

//RequirementTest PositiveTestCase

TEST_F(POWER_HANDLER_TEST, RequirementTest3)
{	 
	STB_ADC_set(300,800);
	m_POW_monitor();
	Get_status(&PowerSts, &BatterySts);
	ASSERT_EQ(BAT_LOW, PowerSts);
}

//RequirementTest PositiveTestCase

TEST_F(POWER_HANDLER_TEST, BatShutToBatNormTest)
{	 
	STB_ADC_set(200, 800);
	m_POW_monitor();
	Get_status(&PowerSts, &BatterySts);
	ASSERT_EQ(BAT_SHUT, PowerSts);
	STB_ADC_set(400, 800);
	m_POW_monitor();
	Get_status(&PowerSts, &BatterySts);
	ASSERT_EQ(BAT_NORM, PowerSts);
}

//RequirementTest PositiveTestCase

TEST_F(POWER_HANDLER_TEST, BatLowToBatNormTest)
{	 
	STB_ADC_set(300, 800);
	m_POW_monitor();
	Get_status(&PowerSts, &BatterySts);
	ASSERT_EQ(BAT_LOW, PowerSts);
	STB_ADC_set(400, 800);
	m_POW_monitor();
	Get_status(&PowerSts, &BatterySts);
	ASSERT_EQ(BAT_NORM, PowerSts);
}

//RequirementTest PositiveTestCase

TEST_F(POWER_HANDLER_TEST, BatShutToBatLowTest)
{	 
	STB_ADC_set(200, 800);
	m_POW_monitor();
	Get_status(&PowerSts, &BatterySts);
	ASSERT_EQ(BAT_SHUT, PowerSts);
	STB_ADC_set(310, 800);
	m_POW_monitor();
	Get_status(&PowerSts, &BatterySts);
	ASSERT_EQ(BAT_LOW, PowerSts);
}
TEST_F(POWER_HANDLER_TEST, PowStsCheckWhenNoBattery_0)
{
	STB_ADC_set(530, 820);
	m_POW_monitor();
	Get_status(&PowerSts, &BatterySts);
	ASSERT_EQ(POW_AC, PowerSts);
	ASSERT_EQ(BAT_DISCONN, BatterySts);

	STB_ADC_set(400, 820);
	m_POW_monitor();
	Get_status(&PowerSts, &BatterySts);
	ASSERT_EQ(POW_AC, PowerSts);
	ASSERT_EQ(BAT_DISCONN, BatterySts);
}

//RequirementTest PositiveTestCase

TEST_F(POWER_HANDLER_TEST, RequirementTest7)
{	 
	STB_ADC_set(300, 820);
	m_POW_monitor();
	Get_status(&PowerSts, &BatterySts);
	ASSERT_EQ(BAT_DISCONN, BatterySts);
}

//RequirementTest PositiveTestCase

TEST_F(POWER_HANDLER_TEST, RequirementTest8)
{	 
	STB_ADC_set(300, 800);
	m_POW_monitor();
	Get_status(&PowerSts, &BatterySts);
	ASSERT_EQ(BAT_CONN, BatterySts);
}

//LogicalBoundaryTest

TEST_F(POWER_HANDLER_TEST, LogicalUpperBoundMaxTest)
{ 
	STB_ADC_set(1024, 1024);
	m_POW_monitor();
	Get_status(&PowerSts, &BatterySts);
	ASSERT_EQ(ES_DEFAULT, PowerSts);
	ASSERT_EQ(ES_DEFAULT, BatterySts);
}
TEST_F(POWER_HANDLER_TEST, LogicalUpperBoundMidTest)
{
	STB_ADC_set(1023, 1023);
	m_POW_monitor();
	Get_status(&PowerSts, &BatterySts);
	ASSERT_EQ(POW_AC, PowerSts);
	ASSERT_EQ(BAT_DISCONN, BatterySts);
}
TEST_F(POWER_HANDLER_TEST, LogicalUpperBoundMinTest)
{
	STB_ADC_set(1022, 1022);
	m_POW_monitor();
	Get_status(&PowerSts, &BatterySts);
	ASSERT_EQ(POW_AC, PowerSts);
	ASSERT_EQ(BAT_DISCONN, BatterySts);
}
//BoundaryTest MinimumValueTest
TEST_F(POWER_HANDLER_TEST, LogicalLowerBoundMidTest)
{
	STB_ADC_set(0, 0);
	m_POW_monitor();
	Get_status(&PowerSts, &BatterySts);
	ASSERT_EQ(BAT_SHUT, PowerSts);
	ASSERT_EQ(BAT_CONN, BatterySts);
}
TEST_F(POWER_HANDLER_TEST, LogicalLowerBoundMaxTest)
{
	STB_ADC_set(1, 1);
	m_POW_monitor();
	Get_status(&PowerSts, &BatterySts);
	ASSERT_EQ(BAT_SHUT, PowerSts);
	ASSERT_EQ(BAT_CONN, BatterySts);
}
 

//BoundaryTest MaximumValueTest

TEST_F(POWER_HANDLER_TEST, PhysicalUpperBoundTest)
{ 
	STB_ADC_set((uint16_t)65535,(uint16_t) 65535);
	m_POW_monitor();
	Get_status(&PowerSts, &BatterySts);
	ASSERT_EQ(POW_AC, PowerSts);
	ASSERT_EQ(BAT_DISCONN, BatterySts);
}

