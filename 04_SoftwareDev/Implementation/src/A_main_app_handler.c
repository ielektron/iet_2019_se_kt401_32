#pragma div
#pragma mul

#include "A_main_app_handler.h"
#include "M_eeprom_handler.h" 
#include "M_SYS_NOTI_hdlr.h"
#include "M_ZONE_intr_hdlr.h"
#include "A_UI_handler.h"
#include "H_lcd_driver.h"
#include "H_keypad_driver.h"
#include "M_led_handler.h" 
#include "r_cg_serial.h" 
#include "M_event_handler.h"
#include "stdlib.h"
#include "string.h" 
#include "M_mode_key_handler.h"
#include "M_ZONE_fire_hdlr.h"
#include "M_GSM_interface_hdlr.h"

#define APP_STARTUP_WAIT 60

#define MODE_CHANGE_IDLE_INT 4

#define ARM_KEY '7'
#define PART_ARM_KEY '9'
#define DISARM_KEY '2'

#define RESET_WAIT_INTERVAL 4
#define LCD_SHOW_INTERVAL 6// 3 sec
#define APP_WAIT_GSM_TIMEOUT 20


#define EVENT_GROUP_SIZE 2
#define EVENT_TYPE_SIZE 3

typedef enum
{
	MS_STARTUP = 0,
	MS_MONITOR,
	MS_PWD,
	MS_UI,
	MS_ALT,
	MS_STOP,
	MS_SHOW,
}E_SYS_MAIN_STATE;

typedef enum
{
	ALT_SENSE_STATE = 0,
	ALT_IN_PRO_STATE,
	FIRE_ALTSTATE,
	SS_END
}E_SYS_SUB_STATE;


extern uint8_t h_u8GSM_tx_flag,h_u8GSM_rx_flag;


E_SYS_MAIN_STATE e_sys_main_state = MS_MONITOR ;//TEST_STATE;(dbg)
E_SYS_SUB_STATE e_sys_sub_state = ALT_SENSE_STATE;
E_SYS_EVENTS E_SYS_curr_event = E_DEFAULT;

int16_t s16App_Timer1_cnt = 0; 
int16_t s16siren_Timer_cnt = 0;

uint8_t ModeKeyPattern = 0;
/********event***************/

const char EventType_str[EVENT_GROUP_SIZE][10] = {"FIRE","ZONE 2"}; 
const char Event_str[EVENT_TYPE_SIZE][10] = {"ALERT","OPEN","SHORT"};
uint8_t EventList[5] = {0};
int8_t EventCnt = 0,CurrEvent = 0;

 
/********event***************/




void Event_Display(int8_t CurrEvent);
void icon_mapper(G_RESP_CODE code); 
void v_main_state_pre_cond_hdlr(E_SYS_MAIN_STATE state);
G_RESP_CODE m_u8get_alt_status(void); 
void APP_AltTrig(void);
void APP_AltStop(void);
void APP_eventManager(E_SYS_EVENTS event,uint8_t sts)
{
	E_SYS_EVENTS command = E_DEFAULT;
	uint8_t response = sts;
	switch(e_sys_main_state)
	{		
		case MS_MONITOR:
		{
			switch(event)
			{
				case E_ALT_TRIG:
				{				 
					E2P_SendSYS_ALT_data(E_ALT_TRIG);
					APP_AltTrig();
					break;
				}
				case E_ENTER_UI:
				{
					e_sys_main_state = MS_PWD;
					E_SYS_curr_event = E_ENTER_UI;
					v_main_state_pre_cond_hdlr(e_sys_main_state);
					
					break;
				}
				case E_DISARM_KEY:
				{
					e_sys_main_state = MS_PWD;
					E_SYS_curr_event = E_DISARM_KEY;
					v_main_state_pre_cond_hdlr(e_sys_main_state);
					
					break;
				}				
				case E_KEY_CHANGE:
				{
					command = E_SMS_TRIG;			 					 
					break;
				}
				default:
				break;
			}		
			break;
		}
		case MS_SHOW:
		case MS_PWD:
		{
			switch(event)
			{
				case E_ALT_STOP:
				{
					if(sts == SUCCESS)
					{
						APP_AltStop();
						command = E_ALT_STOP;
					}
					else if(sts == EXIT)
					{
						e_sys_main_state = MS_ALT;
						v_main_state_pre_cond_hdlr(e_sys_main_state);
					}
					break;
				}
				case E_ENTER_UI:
				{
					if(sts == SUCCESS)
					{
						command = E_UI_IN;
						e_sys_main_state = MS_UI;
						m_UI_Main_PreCond();
					}
					else if(sts == EXIT)
					{
						e_sys_main_state = MS_MONITOR;
						v_main_state_pre_cond_hdlr(e_sys_main_state);
					}
					break;
				}
				case E_KEY_CHANGE:
				{
					if(E_SYS_curr_event == E_ALT_STOP)
					{
						APP_AltStop();
						command = E_ALT_STOP_SMS;						
					}
					else
					{
						command = E_SMS_TRIG;
					}					 
					break;
				}
				case E_ALT_TRIG:
				{
					if(E_SYS_curr_event != E_ALT_STOP)
					{
						E2P_SendSYS_ALT_data(E_ALT_TRIG);
						APP_AltTrig();
					}
					break;
				}
				case E_DISARM_KEY:
				{
					if(sts == SUCCESS)
					{
						m_u8MKEY_sts_set(DISARM_MODE);
					}					
					e_sys_main_state = MS_MONITOR;
					v_main_state_pre_cond_hdlr(e_sys_main_state);
					break;
				}				
				default:
				break;
			}
			break;
		}		 
		case MS_ALT:
		{
			switch(event)
			{
				case E_ALT_STOP:
				{
					APP_AltStop();
					command = E_ALT_STOP;
					break;
				}
				case E_KEY_CHANGE:
				{
					APP_AltStop();
					command = E_ALT_STOP_SMS;
					break;
				}
				case E_ALT_TRIG:
				{	
					if(sts == DEFAULT)
					{
						E2P_SendSYS_ALT_data(E_ALT_TRIG);
					}					
					break;
				}
				default:
				break;
			}
			break;
		}
		case MS_STOP:
		{			 
			if((event == E_ALT_STOP) && (sts == SUCCESS))
			{
				e_sys_main_state = MS_MONITOR;
				v_main_state_pre_cond_hdlr(e_sys_main_state);
				NOTI_hdlr(E_ALT_STOP,SUCCESS);
			}	 
			break;
		}
		case MS_UI:
		{
			if(EXIT == UI_eventManager(event,sts))
			{
				e_sys_main_state = MS_MONITOR;
				v_main_state_pre_cond_hdlr(e_sys_main_state);
				GSM_CommandSend(E_UI_OUT,DEFAULT);
				m_u8ZONE_reset();
			}
			break;
		}
		default:
		break;
	}
	
	if(command != E_DEFAULT)
	{
		GSM_CommandSend(command,response);
	}
}

void a_u8APP_handler(void)
{
	G_RESP_CODE ret = WAITING;
	char *pwd_inp = NULL;
	uint16_t u16_val = 0;
	/**********keypad input part**************/
	uint8_t u8_ret_val = 0;	 
	KEY_TYPE_T u8_KeyPressed = '\0';	 
	get_key(&u8_KeyPressed);
	if(u8_KeyPressed == KEY_RESET)
	{
		lcd_update_hdlr(0,0,NULL,16);
	}
	
	/*******************zone,key monitoring part************/
	if((e_sys_main_state != MS_STARTUP)&&(e_sys_main_state != MS_UI)&&(e_sys_main_state != MS_STOP))
	{				 
		u8_ret_val = m_vMKEY_monitor();
		if(u8_ret_val != DEFAULT_MODE)
		{
			m_u8ZONE_reset();
			APP_eventManager(E_KEY_CHANGE,u8_ret_val);			
		}
		m_vZONE_monitor();
	}
	switch(e_sys_main_state)
	{
		case MS_STARTUP:
		{
			s16App_Timer1_cnt++;
			if(s16App_Timer1_cnt > APP_STARTUP_WAIT)
			{
				s16App_Timer1_cnt = 0;
				e_sys_main_state = MS_MONITOR;
				v_main_state_pre_cond_hdlr(MS_MONITOR);
			}		
			break;
		}
		case MS_MONITOR:
		{
			if(u8_KeyPressed == MENU)
			{
				APP_eventManager(E_ENTER_UI,DEFAULT);
			}			
			else if(u8_KeyPressed == ENTER)
			{
				ModeKeyPattern = 1;
				u8_KeyPressed = '\0';
			}
			if(ModeKeyPattern == 1)
			{
				s16App_Timer1_cnt++;
				if(s16App_Timer1_cnt > MODE_CHANGE_IDLE_INT)
				{
					s16App_Timer1_cnt = 0;
					ModeKeyPattern = 0;
				}
				if(u8_KeyPressed == ARM_KEY)
				{
					m_u8MKEY_sts_set(ARM_MODE);					 
				}				
				else if(u8_KeyPressed == PART_ARM_KEY)
				{
					m_u8MKEY_sts_set(PART_ARM_MODE);
				}
				else if(u8_KeyPressed == DISARM_KEY)
				{
					 APP_eventManager(E_DISARM_KEY,DEFAULT);
				}
			}
			if(EventCnt != 0)
			{
				if(u8_KeyPressed == DOWN)
				{
					CurrEvent++;
					if(CurrEvent >= EventCnt)
					{
						CurrEvent = 0;

					}
					Event_Display(CurrEvent);
				}	
			}
			else
			{
				
			}
			break;
		}
		case MS_PWD:
		{
			ret = u8_get_input_test(MAX_PWD_COUNT,&pwd_inp,u8_KeyPressed);			 		
			if(ret != WAITING)
			{
			if(ret == SUCCESS)
			{
				u16_val = atoi(pwd_inp);
				if(m_u16_get_pwd() == u16_val)
				{				 
					APP_eventManager(E_SYS_curr_event,SUCCESS);					 
				}
				else
				{
					lcd_update_hdlr(0,0," INVALID ",16);
					lcd_update_hdlr(0,1," PASSWORD ",16);
					s16App_Timer1_cnt = LCD_SHOW_INTERVAL;
					e_sys_main_state = MS_SHOW;
				}
				clear_inp(pwd_inp,MAX_LCD_TXT_LEN);	
			}
			else if(ret == EXIT)
			{
				APP_eventManager(E_SYS_curr_event,EXIT);
				 
			}
			else
			{
				APP_eventManager(E_SYS_curr_event,EXIT);
			}
			}
			break;
		}
		case MS_UI:
		{
			ret = a_vUI_handler(u8_KeyPressed);
			if(ret != WAITING)
			{
				if(ret == EXIT)
				{
					e_sys_main_state = MS_MONITOR;
					v_main_state_pre_cond_hdlr(e_sys_main_state);
					GSM_CommandSend(E_UI_OUT,DEFAULT);
					m_u8ZONE_reset();					
				}
			}
			break;
		}
		case MS_ALT:
		{
			if(u8_KeyPressed != '\0')
			{
				if(MENU == u8_KeyPressed)
				{										 
					 					
				}
				else if(MUTE_KEY == u8_KeyPressed)
				{
					 					 
				}	
				if(MENU == u8_KeyPressed)
				{					
					e_sys_main_state = MS_PWD;
					E_SYS_curr_event = E_ALT_STOP;
					v_main_state_pre_cond_hdlr(e_sys_main_state);
					
				}
				if(EventCnt != 0)
				{
					if(u8_KeyPressed == DOWN)
					{
						CurrEvent++;
						if(CurrEvent >= EventCnt)
						{
							CurrEvent = 0;

						}
						Event_Display(CurrEvent);
					}	
				}
				else
				{
					
				}
			}
			if(s16siren_Timer_cnt != 0)
			{
				s16siren_Timer_cnt--;
				if(s16siren_Timer_cnt <= 0)
				{
					APP_eventManager(E_ALT_STOP,DEFAULT);
				}
			}
			break;
		}
		case MS_STOP:
		{	
			s16App_Timer1_cnt++;
			APP_eventManager(E_ALT_STOP,SUCCESS);//TO BE REMOVED IN ORIGINAL CODE
			if(s16App_Timer1_cnt > APP_WAIT_GSM_TIMEOUT)
			{
				s16App_Timer1_cnt = 0;
				/*******##TBH##*********/
			}			
			break;
		}
		case MS_SHOW:
		{
			s16App_Timer1_cnt--;
			if(s16App_Timer1_cnt <= 0)
			{
				e_sys_main_state = MS_PWD;				 
				v_main_state_pre_cond_hdlr(e_sys_main_state);
			}
			break;
		}
		default:
		break;
	}	
} 
void APP_init(void)
{
	e_sys_main_state = MS_STARTUP; 
 	e_sys_sub_state = SS_END;
	E_SYS_curr_event = E_DEFAULT;
	s16App_Timer1_cnt = 0; 
	s16siren_Timer_cnt = 0;
	
	//lcd_update_hdlr(0,0,"  IELEKTRON",16);		 
	//lcd_update_hdlr(0,1," TECHNOLOGIES  ",16);
	lcd_update_hdlr(0,0,"    SYSTEM ",16);		 
	lcd_update_hdlr(0,1," INITIALIZING",16);
	//E2P_SendSYSdata();
}
void v_main_state_pre_cond_hdlr(E_SYS_MAIN_STATE state)
{
	char loc_arr[MAX_LCD_TXT_LEN+1] = {0};	
	lcd_update_hdlr(0,0," ",16);
	lcd_update_hdlr(0,1," ",16);
	switch(state)
	{
		case MS_MONITOR:
		{	
			if(EventCnt != 0)
			{
				sprintf(loc_arr,"Event list %d/%d",CurrEvent+1,EventCnt);
				lcd_update_hdlr(0,0,loc_arr,16); 
				sprintf(loc_arr,"%s - %s",EventType_str[(EventList[CurrEvent]/10)],Event_str[(EventList[CurrEvent]%10)]);
				lcd_update_hdlr(0,1,loc_arr,16);		
			}
			else
			{
				//lcd_update_hdlr(0,0,"  IELEKTRON",16);
				lcd_update_hdlr(0,0," ZIDAS",16);
				lcd_update_hdlr(0,1," ALARM SYSTEMS",16);
				
				if(GSM_OK == u8get_GSM_sts())
				{
					lcd_update_hdlr(15,0," ",1);
				}
				else
				{
					lcd_update_hdlr(15,0,"G",1);
				}
			}
			
			break;
		}
		case MS_ALT:
		{			
			switch(e_sys_sub_state) 
			{
				case ALT_SENSE_STATE:
				{	
					lcd_update_hdlr(0,0," ",16);
					lcd_update_hdlr(0,1," ALERT SENSED",16);
					break;
				}
				case FIRE_ALTSTATE:
				{	
					lcd_update_hdlr(0,0," ",16);
					lcd_update_hdlr(0,1," FIRE_ALERT",16);
					break;
				}
				case ALT_IN_PRO_STATE:
				{	
					if(EventCnt != 0)
					{
						sprintf(loc_arr,"Event list %d/%d",CurrEvent+1,EventCnt);
						lcd_update_hdlr(0,0,loc_arr,16); 
						sprintf(loc_arr,"%s - %s",EventType_str[(EventList[CurrEvent]/10)],Event_str[(EventList[CurrEvent]%10)]);
						lcd_update_hdlr(0,1,loc_arr,16);		
					}
					else
					{
						lcd_update_hdlr(0,0," ",16);
						lcd_update_hdlr(0,1," SYSTEM ALERT  ",16);
					}
					
					break;
				}
				default:
				break;
			}
			break;
		}		
		/*case LPM_STATE:
		{			 
			lcd_update_hdlr(0,0,"   LOW   ",16);
			lcd_update_hdlr(0,1," POWER MODE ",16);
			break;
		}*/
		case MS_UI:
		{
			m_UI_Main_PreCond();
			break;
		}
		/*case SM_STATE:
		{			 
			lcd_update_hdlr(0,1," SHUTDOWN MODE  ",16);
			break;
		}*/
		case MS_PWD:
		{	
			if((E_ALT_STOP == E_SYS_curr_event)/* || (ALT_MUTE == E_SYS_curr_event)*/)
			{
				lcd_update_hdlr(0,0," ",16);
				lcd_update_hdlr(13,1,"PWD",3);	
				lcd_update_hdlr(0,1," ",13);
			}
			else
			{
				lcd_update_hdlr(0,0,"ENTER PASSWORD",16);
				lcd_update_hdlr(0,1," ",16);
			}
			lcd_goToXy(1,2);
			//lcd_cursor_on();
			//lcd_blink_on();
			clear_pwd_inp();
			break;
		}
		/*case RESPONSE_WAIT:
		{
			lcd_update_hdlr(0,0,"    WAITING",16);		 
			break;
		}*/
		default:
		break;
	}
}
void APP_AltTrig(void)
{
	e_sys_main_state = MS_ALT;//ALERT_STATE;
	e_sys_sub_state = ALT_IN_PRO_STATE;
	v_main_state_pre_cond_hdlr(e_sys_main_state);
	NOTI_hdlr(E_ALT_TRIG,SUCCESS);	 
	s16siren_Timer_cnt = (int16_t)mulu((uint8_t)m_UI_get_siren_time(),120);	 
}

void APP_AltStop(void)
{
	e_sys_main_state = MS_STOP;
	v_main_state_pre_cond_hdlr(e_sys_main_state);	 
	s16siren_Timer_cnt = 0;
	ZONE_FIRE_reset();
	m_u8ZONE_reset();
	NOTI_hdlr(E_ALT_STOP,SUCCESS);
} 
void Event_Display(int8_t CurrEvent)
{
	char loc_arr[MAX_LCD_TXT_LEN+1] = {0};
	uint8_t quo = 0,rem = 0;
	if(EventCnt != 0)
	{
		quo = (uint8_t)divuw(EventList[CurrEvent],10);
		rem = moduw(EventList[CurrEvent],10);
		if((quo < EVENT_GROUP_SIZE)&&(rem < EVENT_TYPE_SIZE))
		{
			sprintf(loc_arr,"Event list %d/%d",CurrEvent+1,EventCnt);
			lcd_update_hdlr(0,0,loc_arr,16); 
			sprintf(loc_arr,"%s - %s",EventType_str[quo],Event_str[rem]);
			lcd_update_hdlr(0,1,loc_arr,16);
		}				
	}
}
 void Event_store(DISP_EVENTS_T event,uint8_t ctl)
{	
	char loc_arr[MAX_LCD_TXT_LEN+1] = {0};
	uint8_t quo = 0,rem = 0;	
	uint8_t Event_type = 0,i = 0,t = 0,max_event = 0;	 
	Event_type = (uint8_t)divuw(event,10);
	Event_type = Event_type * 10;
	max_event =  Event_type + 10;	
	 
	for(i = 0;i<EventCnt;i++)
	{
		if(t==0)
		{
			if((EventList[i] >= Event_type) && (EventList[i] < max_event))
			{
				t =1;
				if(i != (EventCnt - 1))
				{
					EventList[i] = EventList[i+1];	
					EventCnt--;
				}
				else
				{
					EventCnt--;
					break;
				}
				
			}
		}
		else
		{
			EventList[i] = EventList[i+1];
		}
	}
	if(EVENT_REPLACE == ctl)
	{
		EventList[EventCnt] = event;
		EventCnt++;
	}
	if(EventCnt == 0)
	{		 
		if(MS_MONITOR == e_sys_main_state)
		{
			v_main_state_pre_cond_hdlr(MS_MONITOR);
			 
		}
		else if(MS_ALT == e_sys_main_state)
		{
			v_main_state_pre_cond_hdlr(MS_ALT);
			 
		}
		
		CurrEvent = 0;
	}
	else
	{
		if(CurrEvent >= EventCnt)
		{
			CurrEvent--;
		}
		if((MS_MONITOR == e_sys_main_state)||(MS_ALT == e_sys_main_state))
		{
			quo = (uint8_t)divuw(EventList[CurrEvent],10);
			rem = moduw(EventList[CurrEvent],10);
			sprintf(loc_arr,"Event list %d/%d",CurrEvent+1,EventCnt);
			lcd_update_hdlr(0,0,loc_arr,16); 
			sprintf(loc_arr,"%s - %s",EventType_str[quo],Event_str[rem]);
			lcd_update_hdlr(0,1,loc_arr,16);
		}		
	}
}
