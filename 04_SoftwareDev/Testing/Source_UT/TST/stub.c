 
/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "H_GPIO_driver.h"
#include "A_UI_handler.h"
#include "stub.h"


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
LED_NAME LED_name = LED_END;
G_CTL_CODES LED_level = NILL;
GPIO_LEVEL MKEY1_VAL = GPIO_HIGH, MKEY2_VAL = GPIO_HIGH;
SYSTEM_SETTINGS sys_data; 

/***********************************************************************************************************************
* Function Name: m_u8LED_ctrl_led
* Description  : This function mocks the m_u8LED_ctrl_led().
* Arguments    : choice - name of the led
				 cmd - to control the led state
* Return Value : None
***********************************************************************************************************************/
void m_u8LED_ctrl_led(LED_NAME choice, G_CTL_CODES cmd)
{
	LED_name = choice;
	LED_level = cmd;
}

/***********************************************************************************************************************
* Function Name: get_led_name
* Description  : This function returns the led info which will be used by the test case.
* Arguments    : None
* Return Value : one value of enum LED_NAME
***********************************************************************************************************************/

LED_NAME get_led_name(void)
{
	LED_NAME ret = LED_name;
	return ret;
}

/***********************************************************************************************************************
* Function Name: get_led_sts
* Description  : This function returns the led info which will be used by the test case.
* Arguments    : None
* Return Value : one value of enum G_CTL_CODES
***********************************************************************************************************************/

G_CTL_CODES get_led_sts(void)
{
	G_CTL_CODES ret = LED_level;
	return ret;
}

/***********************************************************************************************************************
* Function Name: LED_create
* Description  : This function mocks the LED_create().
 
***********************************************************************************************************************/

void LED_create(void)
{
	LED_NAME gchoice = LED_END;
	G_CTL_CODES gcmd = NILL;
}

/***********************************************************************************************************************
* Function Name: GPIO_input_set
* Description  : This function mocks the GPIO_input_set().
 
***********************************************************************************************************************/

void GPIO_input_set(PIN_NAME_E pin_name, GPIO_LEVEL pin_level)
{
	switch (pin_name)
	{
	case PIN_NAME_MKEY1:
	{
		MKEY1_VAL = pin_level;
		break;
	}
	case PIN_NAME_MKEY2:
	{
		MKEY2_VAL = pin_level;
		break;
	}
	default:
		break;
	}
}
/***********************************************************************************************************************
* Function Name: GPIO_input_get
* Description  : This function mocks the GPIO_input_get().
 
***********************************************************************************************************************/

GPIO_LEVEL GPIO_input_get(PIN_NAME_E pin_name)
{
	GPIO_LEVEL ret;
	switch (pin_name)
	{
	case PIN_NAME_MKEY1:
	{
		ret = MKEY1_VAL;
		break;
	}
	case PIN_NAME_MKEY2:
	{
		ret = MKEY2_VAL;
		break;
	}
	default:
		break;
	}
	return ret;
}
/***********************************************************************************************************************
* Function Name: E2P_LOG_STORE
* Description  : This function mocks the E2P_LOG_STORE().
 
***********************************************************************************************************************/

G_RESP_CODE E2P_LOG_STORE(void)
{
	G_RESP_CODE ret = SUCCESS;
	
	return ret;
}
