#ifndef A_UI_HANDLER_H
#define A_UI_HANDLER_H
/***********************************************************************************************************************
Includes
***********************************************************************************************************************/

#include "r_cg_macrodriver.h"
#include "r_cg_userdefine.h"
//#include "A_main_app_handler.h"
#include "M_mode_key_handler.h" 
/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define MAX_PWD_COUNT 5
#define DEFAULT_TRIG 1 //NO - 0;NC - 1
//#define DEFAULT_TRIG 0 //NO - 0;NC - 1

#define G_NO_NC_LEN 2
#define MODE_STR_LEN 4
/***********************************************************************************************************************
Typedef definitions
***********************************************************************************************************************/

typedef struct 
{	
	uint16_t u16_G_pss_wrd;
	uint16_t u16_G_siren_time;
	uint8_t u8_G_siren_state;
	MODE_KEY_TYPE u8_G_system_mode_control;
	SYS_MODE_E u8_G_system_mode;
	
}SYSTEM_SETTINGS;

/***********************************************************************************************************************
Extern variables
***********************************************************************************************************************/

extern SYSTEM_SETTINGS sys_data;

extern const char s8_G_active_isolate[2][3]; 
extern const char s8_G_NO_NC[G_NO_NC_LEN][5]; 
extern const char s8_G_arm_parm[4][MODE_STR_LEN];
/***********************************************************************************************************************
Global functions
***********************************************************************************************************************/

uint8_t a_vUI_handler(char inp);
void UI_sysDATtoArray(uint8_t *arr);
void UI_sysDATtoStruct(uint8_t *arr);
 
uint8_t u8_get_input(char* disp_msg,uint8_t inp_len, char **usr_inp,char input);
uint8_t u8_get_input_test(uint8_t inp_len, char **usr_inp,char input);
void clear_inp(char *buff,uint16_t buff_len);
uint16_t m_u16_get_pwd();
void v_UI_varRESET(void); 
void m_UI_Main_PreCond(void);

uint16_t m_UI_get_siren_time(void);
void clear_pwd_inp(void);
void factory_reset_process();
G_RESP_CODE UI_eventManager(E_SYS_EVENTS event,uint8_t sts);
#endif