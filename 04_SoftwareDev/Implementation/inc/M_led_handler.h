#ifndef M_LED_HANDLER_H
#define M_LED_HANDLER_H

#include "r_cg_macrodriver.h"
#include "r_cg_userdefine.h"

typedef enum
{					//do not change the order
	LED_AC=0,	//IC2_8BITS
	LED_BAT_GOOD,//21
	LED_BAT_BAD,//22
	LED_ARM,
	LED_ALERT,	
	LED_END
}LED_NAME;
 
void h_LED_hdlr(void); 
void m_u8LED_ctrl_led(LED_NAME choice,G_CTL_CODES cmd);
void LED_TestStart(void);

#endif