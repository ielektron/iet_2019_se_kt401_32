/***********************************************************************************************************************
Start
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/

#include "PowerHandlerStub.h"
#include "M_SYS_NOTI_hdlr.h" 
#include "M_ADC_hdlr.h"
#include "A_main_app_handler.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/

/***********************************************************************************************************************
Global variables and functions
**********************************************************************************************************************/

uint16_t AC;
uint16_t BAT;
POW_MODE_T POWER, BATTERY;

/***********************************************************************************************************************
Extern variables
***********************************************************************************************************************/


/***********************************************************************************************************************
* Function Name: STB_ADC_set
* Description  : This function assign adc value to be returned when M_ADC_CHsts_get() is called.
* Arguments    : ac - Value for ADC_AC
				 bat - Value for ADC_BAT
* Return Value : None
***********************************************************************************************************************/

void STB_ADC_set(uint16_t ac, uint16_t bat)
{
	AC = ac;
	BAT = bat;
}

/***********************************************************************************************************************
* Function Name: Get_status
* Description  : This function store the ac and battery status in address passed as function arguement.
* Arguments    : a - pointer to store the power and battery status
				 b - pointer to store the battery presence status
* Return Value : None
***********************************************************************************************************************/

void Get_status(POW_MODE_T * a, POW_MODE_T *b)
{
	*a = POWER;
	*b = BATTERY;
}

/***********************************************************************************************************************
* Function Name: M_ADC_CHsts_get
* Description  : This function store the adc buffer result.
* Arguments    : ch_no - Adc channel channel selection
				 *buffer - address to store the adc buffer result
* Return Value : ADC Status Or invalid parameter
***********************************************************************************************************************/

uint8_t M_ADC_CHsts_get(uint8_t ch_no, uint16_t *buffer)
{
	if (ch_no == ADC_AC)
	{
		*buffer = AC;
	}
	else if (ch_no == ADC_BAT)
	{
		*buffer = BAT;
	}
	return ADC_STABLE;
}

/***********************************************************************************************************************
* Function Name: NOTI_hdlr
* Description  : This function control the external peripherals based on the event passed as argument.
* Arguments    : event - any one value of enum E_SYS_EVENTS
				 status - event status
* Return Value : any one value of G_RESP_CODE
***********************************************************************************************************************/

G_RESP_CODE NOTI_hdlr(E_SYS_EVENTS event, uint8_t status)
{	
	if (status != BAT_DISCONN && status != BAT_CONN)
	{
		POWER = (POW_MODE_T)status;
	}
	else
	{
		BATTERY = (POW_MODE_T)status;
	}
	return DEFAULT;
}

/***********************************************************************************************************************
* Function Name: APP_eventManager
* Description  : This function notifies the current event to the app handler.
* Arguments    : event - any one value of enum E_SYS_EVENTS
				 sts - event status
* Return Value : None
***********************************************************************************************************************/

void APP_eventManager(E_SYS_EVENTS event, uint8_t sts)
{

}

/***********************************************************************************************************************
End
***********************************************************************************************************************/