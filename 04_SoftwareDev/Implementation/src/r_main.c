/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
* No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
* applicable laws, including copyright laws. 
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIESREGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED
* OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY
* LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR
* ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability 
* of this software. By using this software, you agree to the additional terms and conditions found by accessing the 
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2011, 2015 Renesas Electronics Corporation. All rights reserved.
***********************************************************************************************************************/

/***********************************************************************************************************************
* File Name    : r_main.c
* Version      : CodeGenerator for RL78/G13 V2.03.02.01 [15 May 2015]
* Device(s)    : R5F100LE
* Tool-Chain   : CA78K0R
* Description  : This file implements main function.
* Creation Date: 2/3/2019
***********************************************************************************************************************/

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/
/* Start user code for pragma. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_macrodriver.h"
#include "r_cg_cgc.h"
#include "r_cg_port.h"
#include "r_cg_serial.h"
#include "r_cg_adc.h"
#include "r_cg_timer.h"
/* Start user code for include. Do not edit comment generated here */
#include "tsk_cfg.h"
#include "sys_tick.h"
//#include "M_message_queue_handler.h"
#include "H_lcd_driver.h"
#include "M_led_handler.h" 
#include "M_led_handler.h"
#include "A_main_app_handler.h"
#include "M_ADC_hdlr.h"
#include "M_ZONE_intr_hdlr.h"
#include "PIN_CFG.h"
#include "M_GSM_interface_hdlr.h"
#include "M_ZONE_fire_hdlr.h"

#include "H_M95M01_eeprom_driver.h"
#include "H_DS1307_rtc_driver.h"
#include "M_eeprom_handler.h" 
#include "M_rtc_handler.h" 
#include "M_SYS_NOTI_hdlr.h"
/* End user code. Do not edit comment generated here */
#include "r_cg_userdefine.h"

/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
/* Start user code for global. Do not edit comment generated here */
extern uint8_t h_u8GSM_tx_flag,h_u8GSM_rx_flag;
extern volatile uint16_t test_tick;
static uint32_t tick1 = 0;						// System tick
static TaskType *Task_ptr;                 		// Task pointer
static uint8_t TaskIndex = 0;					// Task index
uint8_t NumTasks =0;
 

RTC_DATE_TIME pres_time;
RTC_DATE_TIME verify_time;
uint8_t data_arr[10] = "hi how";
uint8_t cnt_t  =0;
G_RESP_CODE ret;
uint8_t i=0; 
/* End user code. Do not edit comment generated here */
void R_MAIN_UserInit(void);

/***********************************************************************************************************************
* Function Name: main
* Description  : This function implements main function.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void main(void)
{
    R_MAIN_UserInit();
    /* Start user code. Do not edit comment generated here */
     
    R_CSI10_Create();
    R_CSI10_Start();
    R_UART2_Start();
    R_UART0_Start();
    
    #ifdef RTC_TEST
    pres_time.u8Year=19;
    pres_time.u8Month=1;
    pres_time.u8Day=5;
    pres_time.u8DayOfWeek=0;
    pres_time.u8Hour=10;
    pres_time.u8Minute=24;
    pres_time.u8Second=00;
    h_u8RTC_time_set(pres_time);
    while(1)
    {
	    h_u8RTC_time_get(&verify_time);
    }
    #endif
    
    #ifdef E2P_CRI_TEST
     ret = E2P_criticalDataStore();
     if(ret != SUCCESS)
     {
	     E2P_criticalDataRetrieve();
     }
     else
     {
	     E2P_criticalDataRetrieve();
     }
    #endif

    #ifdef E2P_LOG_TEST
    //m_vE2P_clear();
    //E2P_LogReset();
    ret = RTC_update();
    E2P_criticalDataRetrieve();
    if(ret == SUCCESS)
    {
	    for(i = 0;i<10;i++)
	    {
		 ret = E2P_LogStore();
		    if(ret!=SUCCESS)
		    {
			    //E2P_LogRetrieve(LOG_START);
		    }
		    else
		    {
			    while(1)
			    {
				 ret = E2P_LogRetrieve(LOG_START);
				 if(ret == SUCCESS)
				 {
					 break;
				 }
			    }
			    
		    }   
	    }
	    
    }
    #endif
    
    
    h_u8GSM_ctl(ON);
    h_u8GSM_PwrKeyCtl(ON);
    
    R_TAU0_Channel0_Start();   
    R_ADC_Set_OperationOn();
    R_ADC_Start();
    vLCD_BackLight_ctl(ON);
    lcd_init();    
    M_ADC_init();      
    //M_ZONE_control(ZONE_1_FIRE,ON);
    //M_ZONE_control(ZONE_2_FIRE,ON);
    h_u8GSM_ctl(ON);
    h_u8GSM_PwrKeyCtl(ON);
 
    NumTasks = Tsk_GetNumTasks();  
    Task_ptr = Tsk_GetConfig();  
    
    
    //TO initialize A_main_app_handler
    APP_init();
    //TO initialize M_ZONE_fire_hdlr
    ZONE_FIRE_init();
    
    E2P_CRI_RETRIEVE();
     
    
    while (1U)
    {
	    /**********************OS Scheduler Start *****************************/
 	       tick1 = SysTick_GetSystemTick();		// Get current system tick	      
		// Loop through all tasks.  First, run all continuous tasks.  Then,
		// if the number of ticks since the last time the task was run is
		// greater than or equal to the task interval, execute the task.
		for(TaskIndex = 0; TaskIndex < NumTasks; TaskIndex++)
		{
			if((tick1 - Task_ptr[TaskIndex].LastTick) >= Task_ptr[TaskIndex].Interval)
			{
				(*Task_ptr[TaskIndex].Func)();         // Execute Task

				
				Task_ptr[TaskIndex].LastTick = tick1;  // Save last tick the task was ran.
			}
		}
	   /**********************OS Scheduler End *****************************/		
         
    }
    /* End user code. Do not edit comment generated here */
}

/***********************************************************************************************************************
* Function Name: R_MAIN_UserInit
* Description  : This function adds user code before implementing main function.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_MAIN_UserInit(void)
{
    /* Start user code. Do not edit comment generated here */
    EI();
    /* End user code. Do not edit comment generated here */
}

/* Start user code for adding. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */
