#include "M_GSM_interface_hdlr.h"
#include "r_cg_serial.h" 
#include "M_GeneralLib.h"
#include "PIN_CFG.h"
#include "A_UI_handler.h"
#include "M_eeprom_handler.h" 
#include "A_main_app_handler.h"
#include "M_time_out.h"



//#define MAX_LEN_E2P_BUFF 256
#define GSM_UART_TIMEOUT 10
#define MAX_MSG_CHAR_CNT MAX_EEPROM_BUFF_SIZE+5
#define MAX_RETRY_PERIOD 30
#define MAX_RETRY_CNT 5

#define MAX_GSM_DAT_LEN 200

CIRC_BUFF_TYPE GSM_Rx_buff[MAX_CIRC_BUFF_DEPTH];
CIRC_BUFF_ST Rx_GSM_Circ = {GSM_Rx_buff,0,0,MAX_CIRC_BUFF_DEPTH};

GSM_LINE_TYPE line_ptr = GSM_LINE_HEAD_L;

G_RESP_CODE h_u8GSM_Tx_chkdata(GSM_Q_NAME qname);
uint8_t get_tail(void);
int8_t h_u8GSM_CIRCcopy(CIRC_BUFF_ST *circ_buff,uint8_t index,CIRC_BUFF_TYPE *str,uint8_t len);
G_RESP_CODE h_u8GSM_buf_put(CIRC_BUFF_ST *circ_buff,CIRC_BUFF_TYPE *msg_val);
void buff_reset(void);
void h_u8GSM_CIRCflush(void);


//uint8_t E2P_buff[MAX_LEN_E2P_BUFF]={0};
int8_t h_u8GSM_End_cnt = 0;
uint8_t u8GSM_Timer1_cnt =0;
uint8_t u8retry_cnt =0;
uint8_t char_cnt = 0;
uint8_t CmdToBeChkd = E_DEFAULT; 
extern uint8_t h_u8GSM_tx_flag,h_u8GSM_rx_flag;

//uint8_t TEST[100] = {0},test_cnt = 0;


typedef enum
{
	IDLE =0,
	RESPONSE_WAIT,
	RETRY
}E_GSM_TX_STATE;

E_GSM_TX_STATE e_gsm_tx_state = IDLE;

GSM_STS_T u8GSM_pres_state = GSM_NOT_OK;//GSM_NOT_OK

void GSM_interface_hdlr(void)
{
	uint8_t tail = 0;
	uint8_t msg[3];
	uint8_t ret = 0;
	ret = h_u8GSM_Tx_chkdata(GSM_QUEUE);
	if(ret == DATA_AVAILABLE)
	{
		tail = get_tail();
		h_u8GSM_CIRCcopy(&Rx_GSM_Circ,tail,(char *)msg,2);
		switch(msg[0])
		{
			case E_E2P_SEND:
			{
				if(msg[1] == DEFAULT)
				{
					h_u8GSM_CIRCcopy(&Rx_GSM_Circ,tail+2,(char *)EWrite_DataBuffer,MAX_EEPROM_BUFF_SIZE);
					E2P_ReceiveSYSdata(EWrite_DataBuffer);
				}
				else
				{
					APP_eventManager(msg[0],msg[1]);
				}
				break;
			}
			case E_MODE:
			{	
				if(msg[1] < SYS_MODE_TYPE)
				{
					m_u8MKEY_sts_set(msg[1]);
				}
				break;
			}
			case E_GSM_STS:
			{
				if(msg[1] < GSM_STS_END)
				{
					u8GSM_pres_state = msg[1];
				}
				E2P_SendSYSdata();
				break;
			}
			case E_FRST:
			{
				factory_reset_process();
				break;
			} 
			default:
			APP_eventManager(msg[0],msg[1]);
			break;
		}
		if(msg[0]== CmdToBeChkd)
		{
			e_gsm_tx_state= IDLE;
			u8GSM_Timer1_cnt = 0;
			u8retry_cnt = 0;
		}
		buff_reset();
	}
	switch(e_gsm_tx_state)
	{
		case IDLE:
		{
			
			break;	
		}
		case RESPONSE_WAIT:
		{
			u8GSM_Timer1_cnt++;
			if(u8GSM_Timer1_cnt > MAX_GSM_RESP_INT)
			{
			u8GSM_Timer1_cnt = 0;	
			e_gsm_tx_state = RETRY;	
			}
			break;
		}
		case RETRY:
		{
			u8GSM_Timer1_cnt++;
			if(u8GSM_Timer1_cnt > MAX_RETRY_PERIOD)
			{
				GSM_CommandSend(CmdToBeChkd,DEFAULT);	
				u8retry_cnt++;
				if(u8retry_cnt > MAX_RETRY_CNT)
				{
				/************##TBH##*************/
				u8retry_cnt = 0;
				e_gsm_tx_state= IDLE;
				u8GSM_Timer1_cnt = 0;	
				}
			}
			break;
		}
		default:
		break;
		
		
	}
	
}

G_RESP_CODE h_u8GSM_Q_send(GSM_Q_NAME qname,CIRC_BUFF_TYPE *val)
 {
	 G_RESP_CODE ret  = SUCCESS;
	 switch(line_ptr)
	 {
		case GSM_LINE_HEAD_L:
		{
			if(*val == '$')
			{
				line_ptr = GSM_LINE_TAIL_H;
			}
			break;
		}
		case GSM_LINE_TAIL_H:
		{
			if(*val == ';')
			{
				line_ptr=GSM_IDLE;
				h_u8GSM_End_cnt++;		        				
			}
			else if(*val == '$')
			{
				buff_reset();
				line_ptr = GSM_LINE_TAIL_H;
			}
			else
			{
				h_u8GSM_buf_put(&Rx_GSM_Circ,val);
				char_cnt++;
				if(char_cnt >MAX_MSG_CHAR_CNT)
				{
					buff_reset();
				}
			}
			break;
		}
		case GSM_IDLE:
		{
			break;	
		}
		default:
		break;
	 }	  
	 return ret;
 }

void GSM_CommandSend(E_SYS_EVENTS cmd,uint8_t resp)
{	
	uint8_t str[5]={0};	
 	str[0]='$';
	str[1]=cmd;
	str[2]=resp;
	str[3]=';';
	if(CmdToBeChkd != cmd)
	{
		u8retry_cnt = 0;
	}
	CmdToBeChkd = cmd;
	h_u8GSM_tx_flag = INT_IDLE;
	
	#ifdef GSM_UART_SWITCH
	R_UART2_Send(str,5);
	#else
	R_UART0_Send(str,5);
	#endif
		
	//##CH-2-3##
	timeout_chk(&h_u8GSM_tx_flag,6,GSM_UART_TIMEOUT);
	//while(h_u8GSM_tx_flag!=INT_CMPL); 
	/************##TBH##*****TIMEOUT TO BE ADDED******/
	//h_u8GSM_tx_flag = INT_IDLE;
	u8GSM_Timer1_cnt=0;
	//e_gsm_tx_state = RESPONSE_WAIT;
	 
	
	
}

void GSM_CommandSend_Ext(E_SYS_EVENTS cmd,uint8_t resp,uint8_t *data,uint8_t len)
{	
	uint8_t str[MAX_CIRC_BUFF_DEPTH]={0};
	uint8_t i = 3;
	str[0]='$';
	str[1]=cmd;
	str[2]=resp;
	if(len <=MAX_GSM_DAT_LEN)
	{
		for(i = 3;i<len+3;i++)
		{
			str[i] = *data;
			data++;
		}
	}	
	str[i]=';';
	if(CmdToBeChkd != cmd)
	{
		u8retry_cnt = 0;
	}
	CmdToBeChkd = cmd;
	
	#ifdef GSM_UART_SWITCH
	R_UART2_Send(str,len+4);
	#else
	R_UART0_Send(str,len+4);
	#endif
	
	//##CH-2-3##
	timeout_chk(&h_u8GSM_tx_flag,6,GSM_UART_TIMEOUT);
	//while(h_u8GSM_tx_flag!=1);
	/************##TBH##*****TIMEOUT TO BE ADDED******/
	//h_u8GSM_tx_flag = 0;
	u8GSM_Timer1_cnt=0;
	//e_gsm_tx_state = RESPONSE_WAIT;	
}

int8_t h_u8GSM_CIRCcopy(CIRC_BUFF_ST *circ_buff,uint8_t index,CIRC_BUFF_TYPE *str,uint8_t len)
{
    int8_t ret = -1;
    uint8_t t4 = 0;
    uint8_t i = 0,r = 0;
   
    if(circ_buff->tail - circ_buff->head)
    {
	if(index >= circ_buff->maxlen)
        {
            index = index - circ_buff->maxlen;
        }
        if(circ_buff->head > circ_buff->tail)
        {
            if((index < circ_buff->tail)||(index >= circ_buff->head))
            {
                return -1;
            }
            t4 = circ_buff->head;
        }
        else
        {
            if((index < circ_buff->tail)&&(index >= circ_buff->head))
            {
                return -1;
            }
            t4 = circ_buff->maxlen;
        }
        for(i = index;i<t4;i++)
        {
             str[r] = circ_buff->buffer[i];
             r++;
             if(r == len)
             {
                 str[r] = '\0';
                  ret = i;
                  break;
             }
            if(i == circ_buff->maxlen-1)
            {
                i = -1;
                t4 = circ_buff->head;
            }
        }
    }
    str[r] = '\0';
    return ret;
}

G_RESP_CODE h_u8GSM_Tx_chkdata(GSM_Q_NAME qname)
{
	G_RESP_CODE ret = WAITING;
	if(h_u8GSM_End_cnt > 0)
	{
		ret = DATA_AVAILABLE;
	}
	return ret;
}

 G_RESP_CODE h_u8GSM_buf_put(CIRC_BUFF_ST *circ_buff,CIRC_BUFF_TYPE *msg_val)
 {
	G_RESP_CODE ret = SUCCESS;
	uint8_t temp_head = 0;
	temp_head = circ_buff->head;
	circ_buff->buffer[temp_head] = *msg_val;
	if((temp_head+1) >= circ_buff->maxlen)
	{
		if(circ_buff->tail == 0)
		{
			return GSM_RXBUFF_FULL;
		}
		temp_head = 0;
	}
	else
	{
		if(circ_buff->tail == (temp_head+1))
		{
			return GSM_RXBUFF_FULL;
		}
		temp_head = temp_head + 1;
	}
	//circ_buff->buffer[1].data[3] = 't';  for reference
	circ_buff->head = temp_head;
	return ret;
 }

uint8_t get_tail(void)
{
	uint8_t ret = 0;
	ret = Rx_GSM_Circ.tail;
	return ret;
}

void buff_reset(void)
{
	char_cnt = 0;
	line_ptr = GSM_IDLE;
	h_u8GSM_CIRCflush();
	h_u8GSM_End_cnt = 0;
	line_ptr = GSM_LINE_HEAD_L;
}

void h_u8GSM_CIRCflush(void)
{
	Rx_GSM_Circ.tail = Rx_GSM_Circ.head;
	
}

GSM_STS_T u8get_GSM_sts(void)
{
	GSM_STS_T ret = 0;
	ret = u8GSM_pres_state;//GSM_OK;//
	return ret;
}
G_RESP_CODE h_u8GSM_ctl(G_CTL_CODES ctl)
{
	G_RESP_CODE ret = SUCCESS;
	switch(ctl)
	{
		case ON:
		{
			if(ON == GSM_LDO_ENABLE)
			{
				ret = FAILURE;
			}
			else
			{
				GSM_LDO_ENABLE = ON;
			}			
			break;
		}
		case OFF:
		{
			if(OFF == GSM_LDO_ENABLE)
			{
				ret = FAILURE;
			}
			else
			{
				GSM_LDO_ENABLE = OFF;
			}
			break;
		}
		default:
		break;
	}
	return ret;
}
G_RESP_CODE h_u8GSM_PwrKeyCtl(G_CTL_CODES ctl)
{
	G_RESP_CODE ret = SUCCESS;
	switch(ctl)
	{
		case ON:
		{
			GSM_PWR_KEY = ON;
			if(ON == GSM_PWR_KEY)
			{
				ret = FAILURE;
			}
			else
			{
				
			}			
			break;
		}
		case OFF:
		{
			if(OFF == GSM_PWR_KEY)
			{
				ret = FAILURE;
			}
			else
			{
				GSM_PWR_KEY = OFF;
			}
			break;
		}
		default:
		break;
	}
	return ret;
}
 
/*
void h_u8GSM_PowSeq(void)
{
    h_u8GSM_ctl(OFF);
    h_u8GSM_ctl(ON);
    
    h_vDEL_Ms(100);
    h_u8GSM_PwrKeyCtl(OFF);
    h_vDEL_Ms(100);
    h_u8GSM_PwrKeyCtl(ON);
    //h_vDEL_Ms(1000);
    //h_u8GSM_PwrKeyCtl(OFF);
	
}*/