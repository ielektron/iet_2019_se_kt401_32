
/*

filename : gtst_led_handler.cpp
author : rudresh
created at : 2019-04-05 19:02:12.043414

*/
    
//#include "pch.h"
# include "gtest/gtest.h"
extern "C"
{
#include "M_led_handler.h"
#include "M_led_handler.c"
#include "LedHandleStub.h"
#include "LedHandlerStub.c"
}

class LED_HANDLER_TEST : public::testing::Test {
protected:
	uint8_t led_data;
	void SetUp()
	{		
		/**********Precondition******/
		LED_create();
	}
};

//RequirementTest

TEST_F(LED_HANDLER_TEST, LedAcOnCheck)
{	
	/**********Action************/
	m_u8LED_ctrl_led(LED_AC, ON);

	/**********Verify************/	
	GetStatus(&led_data);
	ASSERT_EQ(1, led_data);
}

//RequirementTest

TEST_F(LED_HANDLER_TEST, LedAcOffCheck)
{	 
	/**********Action************/
	m_u8LED_ctrl_led(LED_AC, OFF);

	/**********Verify************/
	GetStatus(&led_data);
	ASSERT_EQ(0, led_data);
}

//RequirementTest

TEST_F(LED_HANDLER_TEST, LedBatGoodOnCheck)
{	 
	/**********Action************/
	m_u8LED_ctrl_led(LED_BAT_GOOD, ON);

	/**********Verify************/
	GetStatus(&led_data);
	ASSERT_EQ(2, led_data);
}

//RequirementTest

TEST_F(LED_HANDLER_TEST, LedBatGoodOffCheck)
{	 
	/**********Action************/
	m_u8LED_ctrl_led(LED_BAT_GOOD, OFF);

	/**********Verify************/
	GetStatus(&led_data);
	ASSERT_EQ(0, led_data);
}

//RequirementTest

TEST_F(LED_HANDLER_TEST, LedBatBadOnCheck)
{ 
	/**********Action************/
	m_u8LED_ctrl_led(LED_BAT_BAD, ON);

	/**********Verify************/
	GetStatus(&led_data);
	ASSERT_EQ(4, led_data);
}


TEST_F(LED_HANDLER_TEST, LedBatBadOffCheck)
{ 
	/**********Action************/
	m_u8LED_ctrl_led(LED_BAT_BAD, OFF);

	/**********Verify************/
	GetStatus(&led_data);
	ASSERT_EQ(0, led_data);
}
 
TEST_F(LED_HANDLER_TEST, LedInvalidCheck)
{
	/**********Action************/
	m_u8LED_ctrl_led(LED_END, ON);

	/**********Verify************/
	GetStatus(&led_data);
	ASSERT_EQ(0, led_data);
}
TEST_F(LED_HANDLER_TEST, LedInvalidCmdCheck)
{
	/**********Action************/
	m_u8LED_ctrl_led(LED_END, NILL);

	/**********Verify************/
	GetStatus(&led_data);
	ASSERT_EQ(0, led_data);
}


