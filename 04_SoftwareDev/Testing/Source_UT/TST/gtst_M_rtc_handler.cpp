
/*

filename : gtst_rtc_handler.cpp
author : rudresh
created at : 2019-04-05 19:41:46.464809

*/
    
# include "gtest/gtest.h"
extern "C"
{
	#include"RtcStub.h"
	#include"RtcHandlerstub.c"
	
	#include"M_rtc_handler.h"
	#include"M_rtc_handler.c"	
	#include"r_cg_userdefine.h"
	#include"M_GeneralLib.h"
	#include"M_GeneralLib.c"
}


class RTC_HANDLER_LOG_TEST : public::testing::Test {
protected:
	uint8_t arr[100];
	void SetUp()
	{
		 
	}
};
 

TEST(RTC_TestCase_RTC_update, CorrectTimeUpdateTest)    //TESTCASE FOR ALL SUCCESSFUL POSSIBLITIES
{	
	setter(31, 05, 18, 12, 45, 44);	
	ASSERT_EQ(SUCCESS,(G_RESP_CODE) RTC_update()); 
}


TEST(RTC_TestCase_RTC_update, SecondsInvalidTest)  //TESTCASE FOR VERIFYING WHETHER THE SECONDS ARE CHECKED ARE NOT(BUG)
{
	setter(31, 05, 18, 12, 45, 234);

	ASSERT_EQ(FAILURE,(G_RESP_CODE)RTC_update());

}

TEST(RTC_TestCase_RTC_update, MinutesInvalidTest)  //MINUTES CHECKING
{
	setter(31, 05, 18, 12, 80, 60);

	ASSERT_EQ(FAILURE, (G_RESP_CODE)RTC_update());

}

TEST(RTC_TestCase_RTC_update, YearZeroTest) //YEAR CHECKING
{
	setter(0, 05, 18, 12, 45, 44);
	
	ASSERT_EQ(FAILURE, (G_RESP_CODE)RTC_update());
}
TEST(RTC_TestCase_RTC_update, YearUpperboundTest_0) //YEAR CHECKING
{
	setter(100, 05, 18, 12, 45, 44);

	ASSERT_EQ(FAILURE, (G_RESP_CODE)RTC_update());
}
TEST(RTC_TestCase_RTC_update, YearUpperboundTest_1) //YEAR CHECKING
{
	setter(99, 05, 18, 12, 45, 44);

	ASSERT_EQ(SUCCESS, (G_RESP_CODE)RTC_update());
}
TEST(RTC_TestCase_RTC_update, YearUpperboundTest_2) //YEAR CHECKING
{
	setter(98, 05, 18, 12, 45, 44);

	ASSERT_EQ(SUCCESS, (G_RESP_CODE)RTC_update());
}
TEST(RTC_TestCase_RTC_update, YearLowerboundTest_0) //YEAR CHECKING
{
	setter(17, 05, 18, 12, 45, 44);

	ASSERT_EQ(FAILURE, (G_RESP_CODE)RTC_update());
}
TEST(RTC_TestCase_RTC_update, YearLowerboundTest_1) //YEAR CHECKING
{
	setter(18, 05, 18, 12, 45, 44);

	ASSERT_EQ(SUCCESS, (G_RESP_CODE)RTC_update());
}
TEST(RTC_TestCase_RTC_update, YearLowerboundTest_2) //YEAR CHECKING
{
	setter(19, 05, 18, 12, 45, 44);

	ASSERT_EQ(SUCCESS, (G_RESP_CODE)RTC_update());
}
TEST(RTC_TestCase_RTC_update, MonthInvalidCheck) //MONTH CHECKING WITH A NEGATIVE VALUE
{
	setter(31, -5, 18, 12, 45, 44);

	ASSERT_EQ(FAILURE, (G_RESP_CODE)RTC_update());
}
TEST_F(RTC_HANDLER_LOG_TEST, ReturnPointerValidCheck)   //VERIFYING WHETHER LOGSTORE RETURNS IMPROPER ARRAY
{	 
	setter(31, 05, 18, 12, 45, 44);	
	ASSERT_EQ(SUCCESS, (G_RESP_CODE)RTC_update());
	ASSERT_EQ(arr + 16, RTC_LogStore(arr));
}

TEST_F(RTC_HANDLER_LOG_TEST, ReturnPointerInvalidCheck)   //VERIFYING WHETHER LOGSTORE RETURNS PROPER ARRAY
{
	setter(31, 05, 18, 12, 45, 44);
	ASSERT_EQ(SUCCESS, (G_RESP_CODE)RTC_update());
	ASSERT_NE(arr + 18, RTC_LogStore(arr));
}

TEST_F(RTC_HANDLER_LOG_TEST, Test3)   //RTC RETURNS A FAILURE AND LOGSTORE RETURNS A SUCCESS
{	 
	setter(31, 05, 18, 12, -5, 44);
	ASSERT_EQ(FAILURE, (G_RESP_CODE)RTC_update());
	ASSERT_EQ(arr + 16, RTC_LogStore(arr));
}

TEST_F(RTC_HANDLER_LOG_TEST, Test4)   //RTC RETURNS A SUCCESS AND LOGSTORE RETURNS A SUCCESS
{ 
	setter(31, 05, 18, 12,16, 44);
	ASSERT_EQ(SUCCESS, (G_RESP_CODE)RTC_update());
	ASSERT_EQ(arr + 16, RTC_LogStore(arr));
}

TEST_F(RTC_HANDLER_LOG_TEST, Test5)   //RTC RETURNS A SUCCESS AND LOGSTORE RETURNS A FAILURE
{	 
	setter(31, 05, 18, 12,34, 44);
	ASSERT_EQ(SUCCESS, (G_RESP_CODE)RTC_update());
	ASSERT_NE(arr + 20, RTC_LogStore(arr));
}
/*
TEST_F(RTC_HANDLER_LOG_TEST, Test6)   //TESTCASE FOR NULL VALIDATION IN LOGSTORE FUNCTION WHICH IS A BUG
{ 
	setter(31, 05, 18, 12, 34, 44);
	ASSERT_EQ(SUCCESS, (G_RESP_CODE)RTC_update());
	ASSERT_EQ(NULL, RTC_LogStore(NULL));
}
*/
TEST_F(RTC_HANDLER_LOG_TEST, StringFormatValidCheck)   //VERIFYING WHETHER LOGSTORE RETURNS PROPER ARRAY
{
	uint8_t *ter_ptr = NULL;
	setter(31, 05, 18, 12, 45, 44);
	ASSERT_EQ(SUCCESS, (G_RESP_CODE)RTC_update());
	ter_ptr = RTC_LogStore(arr);
	*ter_ptr = '\0';
	ASSERT_STREQ("31/5/18,12:45:44", (char*)arr);	
}

TEST(RTC_TestCase_currTimeToArray, Test1)   
{
	uint8_t arr2[200];
	setter(31, 05, 18, 12, 34, 44);
	ASSERT_EQ(SUCCESS, (G_RESP_CODE)RTC_update());
	ASSERT_EQ(SUCCESS, RTC_currTimeToArray(arr2));
}

TEST(RTC_TestCase_currTimeToArray, Test2)   //
{
	uint8_t arr2[200];
	setter(31, 05, 56, 12, 34, 44);

	ASSERT_EQ(FAILURE, (G_RESP_CODE)RTC_update());
	ASSERT_EQ(SUCCESS, RTC_currTimeToArray(arr2));
}

TEST(EVENT_TestCase, test1)
{
	G_RESP_CODE a;
	setter(31, 05, 18, 12, 34, 44);
	RTC_retryManager();
	RTC_retryHandler();
	getstatus(&a);
	ASSERT_EQ(SUCCESS, a);
}

TEST(EVENT_TestCase, test2)  
{
	G_RESP_CODE a;
	setter(31,31, 18, 12, 34, 44);
	RTC_retryManager();
	//resetting rtc for 6 times 
	RTC_retryHandler();
	RTC_retryHandler();
	RTC_retryHandler();
	RTC_retryHandler();
	RTC_retryHandler();
	RTC_retryHandler(); 
	getstatus(&a);
	ASSERT_EQ(FAILURE, a);

}

TEST(RTC_TestCase_Boundarytest, Test1)    //boundary test maximum value
{
	setter(125, 05, 18, 12, 45, 44);

	ASSERT_EQ(SUCCESS, (G_RESP_CODE)RTC_update());
}


TEST(RTC_TestCase_Boundarytest, Test2)    //boundary test minimum value
{
	setter(150,05, 18, 12, 45, 44);

	ASSERT_EQ(FAILURE, (G_RESP_CODE)RTC_update());
}

TEST(RTC_TestCase_Boundarytest, Test3)    //boundary test mid value
{
	setter(0, 05, 18, 12, 45, 44);

	ASSERT_EQ(FAILURE, (G_RESP_CODE)RTC_update());
}

TEST(RTC_TestCase_Boundarytest, Test4)  //boundary test for month with maximum value
{
	setter(18,25, 18, 12, 45, 44);

	ASSERT_EQ(SUCCESS, (G_RESP_CODE)RTC_update());
}

TEST(RTC_TestCase_Boundarytest, Test5)    //boundary test for month with minimum value
{
	setter(18,35, 18, 12, 45, 44);

	ASSERT_EQ(FAILURE, (G_RESP_CODE)RTC_update());
}


TEST(RTC_TestCase_Boundarytest, Test6)   //boundary test for month with mid value
{
	setter(18,0, 18, 12, 45, 44);

	ASSERT_EQ(FAILURE, (G_RESP_CODE)RTC_update());
}


