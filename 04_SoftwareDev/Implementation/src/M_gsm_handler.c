#include "M_gsm_handler.h"
//#include "M_rtc_handler.h"
#include "M_message_queue_handler.h"
#include "sys_peripheral_handler.h"
#include "M_mode_key_handler.h"
#include "H_M66_gsm_driver.h"
#include "r_cg_serial.h"
#include "string.h"
#include "stdio.h"
#include "M_event_handler.h"
#include "M_led_handler.h"
#include "M_GeneralLib.h" 

#define MAX_REC_INTERVAL 60// 30 seconds
#define MAX_CALL_WAIT_INT 100// 50 seconds
#define MAX_SMS_WAIT_INT 20//minimum 4 sec delay needed between two consecutive sms
#define MAX_SMS_SENT_WAIT_INT 8
#define PH_NO_LIST_LEN 10

#define GSM_RESTART_WAIT_INTERVAL 6 


#define GSM_TIME_UPDATE_INTERVAL 20//500 ms units

typedef enum
{
	GSM_IDLE_STATE = 0,
	GSM_RECORD_STATE,
	SEND_CALL_STATE,	 	
	SEND_SMS_STATE,
	GSM_ALERT_STATE,
	GSM_INIT_STATE,
	ALT_STOP_STATE,
	GSM_RECOVERY_STATE,	
	GSM_DISABLE_STATE,
	GSM_INIT_WAIT_STATE
}M_GSM_STATE_T;

typedef enum
{
	IDLE_STATE = 0,
	DEL_REC = 10,
	START_REC_STATE,
	RECORDING,
	STOP_REC_STATE,
	SMS_PREP = 20,
	SENDING_SMS,
	SMS_WAIT_STATE,
	SENT_WAIT_STATE,
	CALL_PREP = 30,
	CALL_SEND,
	CALL_ANS_RETRY,
	CALL_WAIT,
	CALL_HANG_RETRY
}M_GSM_SUB_STATE_T;
typedef struct 
{	
	uint8_t number[PHONE_NO_OFFSET];
	ACTIVE_STS no_state;
	
}NO_SETTINGS;


NO_SETTINGS ph_no[10] = {{"9176731172",ACTIVATE},{"9176731172",ACTIVATE},{"8072500974",ACTIVATE}};

M_GSM_STATE_T e_G_gsm_state = GSM_INIT_WAIT_STATE;//test - GSM_IDLE_STATE
M_GSM_STATE_T e_G_gsm_state_cpy = GSM_IDLE_STATE;
M_GSM_SUB_STATE_T e_G_gsm_substate = IDLE_STATE;//test - IDLE_STATE

static MSGQ_CMD u8cmd2ack = NO_COMMAND;
static int8_t u8GSM_timer1_cnt = 0,u8GSM_time_update_cnt = GSM_TIME_UPDATE_INTERVAL;
static int8_t s8alt_person = -1,s8alt_person_th = PH_NO_LIST_LEN-1,s8alt_person_cpy = -1;
static char *u8msg_ptr = NULL;
 
const char sms_buff_day[] = "ENTERED IN TO DAY MODE";
const char sms_buff_night[] = "ENTERED IN TO NIGHT MODE";
const char sms_buff_disable[] = "SYSTEM COMPLETELY DISABLED";

#ifdef DEBUG_ENABLE
extern uint8_t u8_log_uart_tx_flag;
char debug_arr[DEBUG_LEN]={0};
#endif
 
static G_RESP_CODE m_vGSM_rec_hdlr(void);
static G_RESP_CODE m_vGSM_call_hdlr(APP_MSG_TYPE_ST *msgq);
static G_RESP_CODE m_vGSM_sms_hdlr(APP_MSG_TYPE_ST *msgq); 
static G_RESP_CODE m_u8GSM_msg_resp_hdlr(APP_MSG_TYPE_ST *msgq);
static void m_u8GSM_restore(void);
G_RESP_CODE m_u8GSM_AT_TIMEOUT_cb(uint8_t param);
void m_u8GSM_sms_sleep(void);
void m_u8GSM_sms_wakeup(void);


void m_vGSM_handler(void)
{
	APP_MSG_TYPE_ST loc_msg = {0,MSG_RESPONSE,NO_COMMAND,NO_RESPONSE,NULL};
	G_RESP_CODE ret;
	ret = m_u8MSGQ_msg_receive(&loc_msg,GSM_QUEUE);
	if(ret != MSGQ_EMPTY)
	{
		m_u8GSM_msg_resp_hdlr(&loc_msg);
	}
	/*
	if((e_G_gsm_substate != SENDING_SMS) &&(SMS_WAIT_STATE))
	{
		h_u8GSM_time_get(date_time_arr);
	}*/	
	switch(e_G_gsm_state)
	{
		case GSM_IDLE_STATE:
		{
			u8GSM_time_update_cnt--;
			if(u8GSM_time_update_cnt <= 0)
			{
				//ch-7-10
				ret = periph_sts(GSM_UART);				
				if(GSM_OK == ret)
				{	
					h_u8GSM_time_update();				        
				        ret = m_u8RTC_test(NILL);
				}
				u8GSM_time_update_cnt = GSM_TIME_UPDATE_INTERVAL;
								 
			}
			//m_u8LED_update();
			//m_u8MSGQ_cmd_send(LCD_DBG_CMD,u8GSM_time_update_cnt,FP_QUEUE);
			break;
		}
		case GSM_RECORD_STATE:
		{			 
			ret = m_vGSM_rec_hdlr();
			if(ret != WAITING)
			{
				e_G_gsm_state = GSM_IDLE_STATE;
			}
			break;
		}
		case GSM_ALERT_STATE:
		{
			ret = m_vGSM_call_hdlr(&loc_msg);
			if(ret != WAITING)
			{
				s8alt_person = -1;
				s8alt_person_th = PH_NO_LIST_LEN-1;				 
				e_G_gsm_substate = CALL_PREP;
				e_G_gsm_state = GSM_ALERT_STATE;
			}
			break;
		}
		case SEND_CALL_STATE:
		{
			ret = m_vGSM_call_hdlr(&loc_msg);
			if(ret != WAITING)
			{
				e_G_gsm_state = GSM_IDLE_STATE;
			}
			break;
		}
		case SEND_SMS_STATE:
		{
			ret = m_vGSM_sms_hdlr(&loc_msg);
			if(ret != WAITING)
			{
				e_G_gsm_state = GSM_IDLE_STATE;
			}
			break;
		}
		case GSM_INIT_STATE:
		{
			ret = h_u8GSM_init();			 
			if(ret != WAITING)  
			{
				if(ret == GSM_NO_SIM)
				{				
					e_G_gsm_state = GSM_IDLE_STATE;
				}
				else if(ret == GSM_INT_ERROR)
				{
					e_G_gsm_state = GSM_IDLE_STATE;
					ret = GSM_INT_ERROR;
				}
				else if(ret == GSM_OK)
				{
					e_G_gsm_state = GSM_IDLE_STATE;
				}
				else if(GSM_UART_ERROR == ret)
				{
					
				}
				else
				{
					/******need to be handled******/
				}
				e_G_gsm_state = GSM_IDLE_STATE;
				e_G_gsm_substate = IDLE_STATE;
				periph_sts_set(GSM_UART,ret);
				m_u8MSGQ_cmd_send(GSM_STS,ret,MAPP_QUEUE);
				m_u8MSGQ_cmd_send(GSM_STS,ret,FP_QUEUE);
			}
			break;
		}
		case ALT_STOP_STATE:
		{
			ret = h_u8GSM_call_hang();			 
			if(ret!=WAITING)
			{
				if(ret == SUCCESS)
				{
					m_u8GSM_restore();
					m_u8MSGQ_resp_send(ALT_STOP,SUCCESS,MAPP_QUEUE);					 
				}
				else
				{
					m_u8MSGQ_resp_send(ALT_STOP,FAILURE,MAPP_QUEUE);
				}
				e_G_gsm_state = GSM_IDLE_STATE;
				e_G_gsm_substate = IDLE_STATE;
			}
			break;
		}
		case GSM_RECOVERY_STATE:
		{
			u8GSM_timer1_cnt--;
			if(u8GSM_timer1_cnt <= 0)
			{
				h_u8GSM_ctl(ON);
				e_G_gsm_state = GSM_INIT_STATE;	
				periph_sts_set(GSM_UART,GSM_INITIALIZING);
			}
			break;
		}
		case GSM_DISABLE_STATE:
		{
			break;
		}
		case GSM_INIT_WAIT_STATE:
		{
			break;
		}
		default:
		break;
	}	
}
static G_RESP_CODE m_vGSM_rec_hdlr(void)
{
	G_RESP_CODE ret = WAITING,g_ret = WAITING;	  
	switch(e_G_gsm_substate)
	{
		case DEL_REC:
		{
			ret = h_u8GSM_record_del();
			if(ret != WAITING)
			{
				ret = WAITING;
				u8GSM_timer1_cnt = 0;
				e_G_gsm_substate = START_REC_STATE;
			}			 	
			break;
		}
		case START_REC_STATE:
		{
			ret = h_u8GSM_record_start();
			if(ret!= WAITING)
			{				 
				if(ret == SUCCESS)
				{
					m_u8MSGQ_resp_send(START_REC,SUCCESS,FP_QUEUE);
					e_G_gsm_substate =RECORDING;
					u8GSM_timer1_cnt = 0;				 
				}
				else
				{
					e_G_gsm_substate = STOP_REC_STATE;
				}
				ret = WAITING;
			}			 
			break;
		}
		case RECORDING:
		{
			u8GSM_timer1_cnt++; 
			if(u8GSM_timer1_cnt > MAX_REC_INTERVAL)
			{
				u8GSM_timer1_cnt = 0;
				e_G_gsm_substate = STOP_REC_STATE;	
			}
			else
			{
				break;
			}			
		}
		case STOP_REC_STATE:
		{
			ret = h_u8GSM_record_stop();
			if(ret!=WAITING)
			{
				m_u8MSGQ_resp_send(STOP_REC,ret,FP_QUEUE);				 
				u8GSM_timer1_cnt = 0;
				e_G_gsm_substate =IDLE_STATE;				 
				g_ret = SUCCESS;
			}			
			break;
		}
		default:
		break;
	}
	return g_ret;
}
static G_RESP_CODE m_vGSM_call_hdlr(APP_MSG_TYPE_ST *msgq)
{
	G_RESP_CODE ret = WAITING,g_ret = WAITING;
	switch(e_G_gsm_substate)
	{
		case CALL_PREP:
		{			 
			if(s8alt_person < s8alt_person_th)
			{
				s8alt_person++;
				if(ph_no[s8alt_person].no_state == ACTIVATE)
				{
					e_G_gsm_substate =  CALL_SEND;
				}				
			}
			else
			{
				s8alt_person = -1;
				s8alt_person_th = 0;
				g_ret = SUCCESS;
				if(u8cmd2ack == SEND_CALL)
				{
					m_u8MSGQ_resp_send(SEND_CALL,SUCCESS,FP_QUEUE);					 
					u8cmd2ack = NO_COMMAND;		
				}
				
			}		
			break;
		}
		case CALL_SEND:
		{	
			#if 1
			ret = h_u8GSM_call_send(ph_no[s8alt_person].number);
			if(ret != WAITING)
			{
				if(ret == SUCCESS)
				{
					u8GSM_timer1_cnt = 0;
					e_G_gsm_substate = CALL_WAIT;				
				}
				else
				{
					h_u8GSM_call_hang();
					e_G_gsm_substate = CALL_PREP;
					/***error need to be handled******/
					#ifdef DEBUG_ENABLE				 
					sprintf(debug_arr,"ATD FAIL\r");
					R_UART1_Send((uint8_t *)debug_arr,DEBUG_LEN);
					while(!u8_log_uart_tx_flag);
					u8_log_uart_tx_flag = 0;
					#endif
				}
			}
			#else
			#ifdef DEBUG_ENABLE				 
			sprintf(debug_arr,"call ok\r");
			R_UART1_Send((uint8_t *)debug_arr,strlen(debug_arr)+1);
			while(!u8_log_uart_tx_flag);
			u8_log_uart_tx_flag = 0;
			#endif
			
			e_G_gsm_substate = CALL_PREP;
			ret = WAITING;
			#endif
			break;
		}
		case CALL_ANS_RETRY:
		{
			ret = h_u8GSM_CallPlayBack();
			if(ret!=WAITING)
			{				 
				e_G_gsm_substate = CALL_WAIT;				 
			}
			/*****break intentionally left***********/
		}
		case CALL_WAIT:
		{			
			u8GSM_timer1_cnt++;		  
			   ret = h_u8GSM_call_resp_hdlr();		   
			   if(ret == CALL_ESTB)
			   {
				 				   
				   
			   }
			   else if(ret == CALL_ANS_ERROR)
			   {				    
				   e_G_gsm_substate = CALL_ANS_RETRY;		     			 
			   }
			   else if(ret == CALL_ANS)
			   {				    
				u8GSM_timer1_cnt =  MAX_CALL_WAIT_INT - MAX_REC_INTERVAL;			     			 
			   }
			   else if((ret == CALL_HANG) || (u8GSM_timer1_cnt > MAX_CALL_WAIT_INT))
			   {				    			    
				 e_G_gsm_substate = CALL_HANG_RETRY;
				 ret = CALL_HANG;
			   }
			   if(ret != WAITING)
			   {
				 msgq->command = CALL_STS;			 				  
				 msgq->response = ret;
				 ret = m_u8MSGQ_msg_send(msgq,FP_QUEUE); 
				 h_u8GSM_CIRCflush();
			   }			    		 
			break;
		}
		case CALL_HANG_RETRY:
		{
		   	ret = h_u8GSM_call_hang();
			if(WAITING != ret)
			{				 				    
				 e_G_gsm_substate = CALL_PREP;
				 ret = CALL_HANG;
				 u8GSM_timer1_cnt = 0;
				 msgq->command = CALL_STS;		 				  
				 msgq->response = ret;
				 ret = m_u8MSGQ_msg_send(msgq,FP_QUEUE);
			}		   	
			break;
		}
		default:
		break;
	}
	return g_ret;
}
static G_RESP_CODE m_vGSM_sms_hdlr(APP_MSG_TYPE_ST *msgq)
{
	G_RESP_CODE ret = WAITING,G_ret = WAITING;
	switch(e_G_gsm_substate)
	{
		case SMS_PREP:
		{
			if(s8alt_person < s8alt_person_th)
			{
				s8alt_person++;
				if(ph_no[s8alt_person].no_state == ACTIVATE)
				{
					e_G_gsm_substate =  SENDING_SMS;
				}				
			}
			else
			{
				s8alt_person = -1;
				s8alt_person_th = 0;
				G_ret = SUCCESS;
				m_u8MSGQ_cmd_send(ICON_STS,periph_sts(GSM_UART),FP_QUEUE);				 
			}
			break;
		}
		case SENDING_SMS:
		{
			#if 1		 
			ret = h_u8GSM_sms_send(ph_no[s8alt_person].number,(uint8_t*)u8msg_ptr);
			if(ret!=WAITING)
			{
				if(SUCCESS == ret)
				{
					e_G_gsm_substate = SMS_WAIT_STATE;
					m_u8MSGQ_cmd_send(ICON_STS,MSMS_WAIT,FP_QUEUE);
					u8GSM_timer1_cnt = 0;
				}	
				else
				{
					e_G_gsm_substate = SMS_PREP;					
				}				 
			}
			#else
			/***error need to be handled******/
			#ifdef DEBUG_ENABLE				 
			sprintf(debug_arr,"%s\r",u8msg_ptr);
			R_UART1_Send((uint8_t *)debug_arr,strlen(debug_arr));
			while(!u8_log_uart_tx_flag);
			u8_log_uart_tx_flag = 0;
			#endif
			
			e_G_gsm_substate = SMS_PREP;
			ret = WAITING;
			
			#endif
			break;
		}
		case SMS_WAIT_STATE:
		{			
			ret = sms_resp_handler(0);			 
			u8GSM_timer1_cnt++;
			if((ret != WAITING) || (u8GSM_timer1_cnt >=MAX_SMS_WAIT_INT))
			{
				e_G_gsm_substate = SENT_WAIT_STATE;				
				if(u8GSM_timer1_cnt >=MAX_SMS_WAIT_INT)
				{
					//m_u8MSGQ_cmd_send(LCD_DBG_CMD,10,FP_QUEUE);
				}
				u8GSM_timer1_cnt = 0;		
			}			 
			break;
		}
		case SENT_WAIT_STATE:
		{
			u8GSM_timer1_cnt++;
			if(u8GSM_timer1_cnt >=MAX_SMS_SENT_WAIT_INT)
			{				
				e_G_gsm_substate = SMS_PREP;				 
				u8GSM_timer1_cnt = 0;
				h_u8GSM_CIRCflush();
			}
			break;
		}		 
		default:
		break;
	}
	return G_ret;
}

 
static G_RESP_CODE m_u8GSM_msg_resp_hdlr(APP_MSG_TYPE_ST *msgq)
{
	G_RESP_CODE ret = WAITING;
	//ch-29
	MSGQ_NAME_T Q_name = END_QUEUE;	
	switch(e_G_gsm_state)
	{
		case GSM_INIT_STATE:		 
		case GSM_RECOVERY_STATE:
		{
			break;
		}
		case GSM_INIT_WAIT_STATE:
		{
			if(msgq->command == GSM_INIT_OK)
			{
				e_G_gsm_state = GSM_IDLE_STATE;
				h_u8GSM_at_reset();
			}
			break;
		}
		default:
		{
			switch(msgq->command)
			{
				case START_REC:
				{
					Q_name = FP_QUEUE;
					msgq->response = FAILURE;
					if(e_G_gsm_state == GSM_IDLE_STATE)
					{
						msgq->response = WAITING;
						e_G_gsm_state = GSM_RECORD_STATE;
						e_G_gsm_substate = DEL_REC;
						h_u8GSM_at_reset();
					}			 
					break;
				}
				case STOP_REC:
				{
					Q_name = FP_QUEUE;
					msgq->response = FAILURE;
					if(e_G_gsm_state == GSM_RECORD_STATE)
					{
						msgq->response = WAITING;
						if(RECORDING == e_G_gsm_substate)
						{
							u8GSM_timer1_cnt = MAX_REC_INTERVAL;
						}
						else
						{
							h_u8GSM_at_reset();
							e_G_gsm_substate = STOP_REC_STATE;							 
						}
						
					}
					break;
				}
				case SEND_CALL:
				{
					Q_name = FP_QUEUE;
					msgq->response = FAILURE;
					if(e_G_gsm_state == GSM_IDLE_STATE)
					{
						msgq->response = WAITING;
						s8alt_person = -1;
						s8alt_person_th = 0;
						e_G_gsm_state = SEND_CALL_STATE;
						e_G_gsm_substate = CALL_PREP;						 
						u8cmd2ack = msgq->command;	
						h_u8GSM_at_reset();
					}
					break;
				}		 
				case SEND_SMS:
				{
					Q_name = END_QUEUE;
					if(e_G_gsm_state == GSM_IDLE_STATE)
					{				 
						e_G_gsm_state = SEND_SMS_STATE;
						e_G_gsm_substate = SMS_PREP;				 
					}
					else if((e_G_gsm_substate == SENDING_SMS) || (ALT_STOP_STATE == e_G_gsm_state))
					{				 
						m_u8MSGQ_msg_undo(GSM_QUEUE);
						msgq->response = WAITING;						 
					}
					if(e_G_gsm_substate != SENDING_SMS)
					{				
						if((e_G_gsm_state == GSM_IDLE_STATE) || (e_G_gsm_state == SEND_SMS_STATE))
						{							 
							s8alt_person = -1;
							s8alt_person_th = PH_NO_LIST_LEN-1;					 
							if(NIGHT_MODE == msgq->response)
							{
								u8msg_ptr = sms_buff_night;
							}
							else if(DAY_MODE == msgq->response)
							{
								u8msg_ptr = sms_buff_day;
							}
							else if(DISABLE_MODE == msgq->response)
							{
								u8msg_ptr = sms_buff_disable;
							}					
							else
							{
								msgq->response = FAILURE;
							}					 
						}
					}
					break;
				}	
				case GSM_ENABLE:
				{	
					if(GSM_DISABLE_STATE == e_G_gsm_state)
					{
						ret = h_u8GSM_ctl(ON);				 
						m_u8GSM_restore();
						e_G_gsm_state = GSM_INIT_STATE;
						
					}	
					else
					{
						//m_u8MSGQ_cmd_send(LCD_DBG_CMD,0,FP_QUEUE);				
					}
					break;
				}
				case GSM_RESTART:
				{	
					if(GSM_DISABLE_STATE != e_G_gsm_state)
					{
						u8GSM_timer1_cnt = GSM_RESTART_WAIT_INTERVAL;
					        e_G_gsm_state = GSM_RECOVERY_STATE;
					}
					else
					{
						break;
					}
								 
					/*******break intentionally_left*/
				}
				case GSM_DISABLE:
				{			
					h_u8GSM_ctl(OFF);
					m_u8GSM_restore();		
					periph_sts_set(GSM_UART,GSM_OFF);
					m_u8MSGQ_cmd_send(GSM_STS,GSM_OFF,MAPP_QUEUE);
					m_u8MSGQ_cmd_send(GSM_STS,GSM_OFF,FP_QUEUE);
					
					if(GSM_RECOVERY_STATE != e_G_gsm_state)
					{
						e_G_gsm_state = GSM_DISABLE_STATE;
					}
					break;
				}
				case ALT_TRIG:
				{					
					if(e_G_gsm_state == GSM_IDLE_STATE)
					{
						s8alt_person = -1;
						s8alt_person_th = PH_NO_LIST_LEN-1;
						e_G_gsm_state = GSM_ALERT_STATE;
						e_G_gsm_substate = CALL_PREP;
						h_u8GSM_at_reset();
					}
					else if(e_G_gsm_state == SEND_SMS_STATE)
					{
						if(e_G_gsm_substate == SENDING_SMS)
						{					 
							m_u8MSGQ_msg_undo(GSM_QUEUE);
							msgq->response = WAITING;
						}				
						else
						{
							s8alt_person = -1;
							s8alt_person_th = PH_NO_LIST_LEN-1;
							e_G_gsm_state = GSM_ALERT_STATE;
							e_G_gsm_substate = CALL_PREP;
							msgq->response = SUCCESS;
							h_u8GSM_at_reset();
						}
						
					}
					else if(e_G_gsm_state == GSM_INIT_STATE)
					{
						m_u8MSGQ_msg_undo(GSM_QUEUE);
						msgq->response = WAITING;
					}
					else
					{
						
					}					 
					break;
				}
				case ALT_STOP:
				{
					#if 1
					e_G_gsm_state = ALT_STOP_STATE;
					#else
					msgq->response = SUCCESS;
					#endif
					break;
				}
				case UI_IN:
				{
					m_u8GSM_sms_sleep();
					break;
				}
				case UI_OUT:
				{
					m_u8GSM_sms_wakeup();
					break;
				}
				default:
				break;
			}
		}
		break;	 
	}
	m_u8MSGQ_resp_send(msgq->command,msgq->response,Q_name);	 
	return ret;
}
void m_u8GSM_restore(void)
{	 
        u8cmd2ack = NO_COMMAND;
 	u8GSM_timer1_cnt = 0;
 	s8alt_person = -1;
	s8alt_person_th = 0;
	u8msg_ptr = NULL;
	h_u8GSM_at_reset();
	
}
void m_u8GSM_time_get(char *date_arr)
{
	h_u8GSM_time_get(date_arr);
}
G_RESP_CODE m_u8fill_ph_details(uint8_t *arr)
{
	G_RESP_CODE ret = FAILURE;
	const uint8_t *buff_ptr = 0;
	uint8_t l=0;
	uint8_t i=0;
	buff_ptr = arr + PHONE_NO_START_INDEX;
	 
	for(i=0;i<PH_NO_LIST_LEN;i++)
	{
		for(l=0;l<PHONE_NO_OFFSET;l++)
		{
			ph_no[i].number[l] = *(buff_ptr++);
		}
	}
	buff_ptr = arr + PH_NO_STATUS_START_INDEX;
	for(i=0;i<PH_NO_LIST_LEN;i++)
	{
		ph_no[i].no_state =*(buff_ptr++);
	}
	
	return ret;
}
G_RESP_CODE m_u8GSM_AT_TIMEOUT_cb(uint8_t param)
{
	G_RESP_CODE ret = SUCCESS;
	ret = m_u8MSGQ_cmd_send(GSM_RESTART,NO_RESPONSE,GSM_QUEUE);	 
	m_u8MSGQ_cmd_send(LCD_DBG_CMD,1,FP_QUEUE);
	vBP_IND_set(BP_IND_GSM_TIMEOUT);
	return ret;
}
void m_u8GSM_startup(void)
{
	h_u8GSM_PowSeq();
	h_u8GSM_AT_TIMEOUT_CbRegister(m_u8GSM_AT_TIMEOUT_cb);
	periph_sts_set(GSM_UART,GSM_INITIALIZING);
}
void m_u8GSM_sms_sleep(void)
{
	e_G_gsm_state_cpy = e_G_gsm_state;
	e_G_gsm_state = GSM_IDLE_STATE;
	s8alt_person_cpy = s8alt_person;
	h_u8GSM_at_reset();
}
void m_u8GSM_sms_wakeup(void)
{
	if(SEND_SMS_STATE == e_G_gsm_state_cpy)
	{
		e_G_gsm_state = e_G_gsm_state_cpy;
		s8alt_person = s8alt_person_cpy;
		s8alt_person_th = PH_NO_LIST_LEN-1;
		e_G_gsm_substate = SMS_PREP;
	}
	h_u8GSM_at_reset();
}