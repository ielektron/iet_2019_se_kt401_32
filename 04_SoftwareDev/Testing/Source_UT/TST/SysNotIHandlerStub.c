/***********************************************************************************************************************
Start
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/

#include "SysNotIHandlerStub.h"
#include "M_led_handler.h"
#include "M_SYS_NOTI_hdlr.h"
#include "A_UI_handler.h"

/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/

G_CTL_CODES RELAYSOUND, RELAYEXIT, GOOD , BAD , AC , ALERT;
SYSTEM_SETTINGS sys_data;

/***********************************************************************************************************************
Extern variables
***********************************************************************************************************************/

/***********************************************************************************************************************
* Function Name: init
* Description  : This function initializes global variable with NILL.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/

void init()
{
	RELAYSOUND = RELAYEXIT = GOOD = BAD = AC = ALERT = NILL;
}

/***********************************************************************************************************************
* Function Name: setter
* Description  : This function initializes u8_G_siren_state.
* Arguments    : set - value to be stored in u8_G_siren_state
* Return Value : None
***********************************************************************************************************************/

void setter(uint8_t set)
{
	sys_data.u8_G_siren_state = set;
}

/***********************************************************************************************************************
* Function Name: GetStatus
* Description  : This function assign the global variable values in RELAYLED.
* Arguments    : a - address of the structure RELAYLED
* Return Value : None
***********************************************************************************************************************/

void GetStatus(RELAYLED *a)
{
	a->RELAY_S = RELAYSOUND;
	a->RELAY_E = RELAYEXIT;
	a->LED_GOOD = GOOD;
	a->LED_BAD = BAD;
	a->LED_AC = AC;
	a->LED_ALERT = ALERT;
}

/***********************************************************************************************************************
* Function Name: m_u8LED_ctrl_led
* Description  : This function turn on or off the led specified in functions first argument.
* Arguments    : choice - any one value of enum LED_NAME
				 cmd - on/off
* Return Value : None
***********************************************************************************************************************/

void m_u8LED_ctrl_led(LED_NAME choice, G_CTL_CODES cmd)
{
	if (choice == LED_BAT_GOOD)
	{
		GOOD = cmd;
	}
	else if (choice == LED_BAT_BAD)
	{
		BAD = cmd;
	}
	else if (choice == LED_AC)
	{
		AC = cmd;
	}
	else if (choice == LED_ALERT)
	{
		ALERT = cmd;
	}
}

/***********************************************************************************************************************
* Function Name: M_NOTI_RELAYCtl
* Description  : This function turn on or off the relay specified in functions first argument.
* Arguments    : type - any one value of enum RELAY_TYPE_T
				 ctl - on/off
* Return Value : None
***********************************************************************************************************************/

void M_NOTI_RELAYCtl(RELAY_TYPE_T type, G_CTL_CODES ctl)
{
	if (type == REL_SOUNDER)
	{
		RELAYSOUND = ctl;
	}
	else if (type == REL_DIALER)
	{
		RELAYEXIT = ctl;
	}
}

/***********************************************************************************************************************
End
***********************************************************************************************************************/