#ifndef M_GSM_HANDLER_H
#define M_GSM_HANDLER_H
#include "r_cg_macrodriver.h"
#include "r_cg_userdefine.h"

#define GSM_TIME_LEN 18



void m_vGSM_handler(void);
G_RESP_CODE m_u8fill_ph_details(uint8_t *arr);
void m_u8GSM_time_get(char *date_arr);
void m_u8GSM_startup(void);
 
#endif